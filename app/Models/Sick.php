<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Model\TypeSick;

class Sick extends Model
{
    protected $table = 'sick';

    protected $fillable = [
        'name', 'sick_type_id', 'image', 'introduce'
    ];

    public function sick_type()
    {
        return $this->belongsTo(TypeSick::class, 'sick_type_id', 'id');
    }
}
