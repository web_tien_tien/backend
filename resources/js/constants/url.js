/*
 |--------------------------------------------------------------------------
 | Auth
 |--------------------------------------------------------------------------
 */
export const API_LOGIN = '/api/auth/login'
export const API_LOGOUT = '/api/auth/logout'
export const API_PROFILE = '/api/auth/me'

/*
 |--------------------------------------------------------------------------
 | Permission
 |--------------------------------------------------------------------------
 */
export const API_PERMISSION_LISTING = '/api/admin/permission/listing'
export const API_PERMISSION_DELETE = '/api/admin/permission/delete'
export const API_PERMISSION_STORE = '/api/admin/permission/store'
export const API_PERMISSION_EDIT = '/api/admin/permission/edit'

/*
 |--------------------------------------------------------------------------
 | Role
 |--------------------------------------------------------------------------
 */
export const API_ROLE_LISTING = '/api/admin/role/listing'
export const API_ROLE_DELETE = '/api/admin/role/delete'
export const API_ROLE_STORE = '/api/admin/role/store'
export const API_ROLE_EDIT = '/api/admin/role/edit'
export const API_ROLE_GET_PERMISSION_LIST = '/api/admin/role/getpermissionlist'

/*
 |--------------------------------------------------------------------------
 | User
 |--------------------------------------------------------------------------
 */
export const API_USER_LISTING = '/api/admin/user/listing'
export const API_USER_DELETE = '/api/admin/user/delete'
export const API_USER_STORE = '/api/admin/user/store'
export const API_USER_EDIT = '/api/admin/user/edit'
export const API_USER_UPDATE_PASSWORD = '/api/admin/user/update-password'
export const API_USER_GET_ROLE_LIST = '/api/admin/user/get-role-list'
export const API_USER_UPDATE_PROFILE = '/api/general/user/update-profile'

/*
 |--------------------------------------------------------------------------
 | Actions Log
 |--------------------------------------------------------------------------
 */
export const API_ACTIONS_LOG_LISTING = '/api/admin/actions-log/listing'

/*
 |--------------------------------------------------------------------------
 | Medical
 |--------------------------------------------------------------------------
 */
export const API_MEDICAL_LISTING = '/api/ctv/medical/listing'
export const API_MEDICAL_DELETE = '/api/ctv/medical/delete'
export const API_MEDICAL_STORE = '/api/ctv/medical/store'
export const API_MEDICAL_EDIT = '/api/ctv/medical/edit'
export const API_MEDICAL_GET_MEDICAL_TYPE_LIST = '/api/ctv/medical/get-medical-type-list'
export const API_MEDICAL_GET_COMPANY_LIST = '/api/ctv/medical/get-company-list'

/*
 |--------------------------------------------------------------------------
 | Medical Type
 |--------------------------------------------------------------------------
 */
export const API_MEDICAL_TYPE_LISTING = '/api/ctv/medical-type/listing'
export const API_MEDICAL_TYPE_DELETE = '/api/ctv/medical-type/delete'
export const API_MEDICAL_TYPE_STORE = '/api/ctv/medical-type/store'
export const API_MEDICAL_TYPE_EDIT = '/api/ctv/medical-type/edit'

/*
 |--------------------------------------------------------------------------
 | Disease Type
 |--------------------------------------------------------------------------
 */
export const API_DISEASE_TYPE_LISTING = '/api/ctv/disease-type/listing'
export const API_DISEASE_TYPE_DELETE = '/api/ctv/disease-type/delete'
export const API_DISEASE_TYPE_STORE = '/api/ctv/disease-type/store'
export const API_DISEASE_TYPE_EDIT = '/api/ctv/disease-type/edit'

/*
 |--------------------------------------------------------------------------
 | Company
 |--------------------------------------------------------------------------
 */
export const API_COMPANY_LISTING = '/api/ctv/company/listing'
export const API_COMPANY_DELETE = '/api/ctv/company/delete'
export const API_COMPANY_STORE = '/api/ctv/company/store'
export const API_COMPANY_EDIT = '/api/ctv/company/edit'

/*
 |--------------------------------------------------------------------------
 | Sick
 |--------------------------------------------------------------------------
 */
export const API_SICK_LISTING = '/api/ctv/sick/listing'
export const API_SICK_DELETE = '/api/ctv/sick/delete'
export const API_SICK_STORE = '/api/ctv/sick/store'
export const API_SICK_EDIT = '/api/ctv/sick/edit'
export const API_SICK_GET_SICK_TYPE_LIST = '/api/ctv/sick/get-sick-type-list'