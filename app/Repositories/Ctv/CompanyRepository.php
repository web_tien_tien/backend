<?php

namespace App\Repositories\Ctv;

use App\Model\Company;
use App\Repositories\BaseRepository;

class CompanyRepository extends BaseRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return Company::class;
    }

    /**
     * @param null $keyword
     * @param bool $counting
     * @param int $limit
     * @param int $offset
     * @param string $orderBy
     * @param string $orderType
     * @return mixed
     */
    public function getList($keyword = null, $counting = false, $limit = 10, $offset = 0, $orderBy = 'name', $orderType = 'asc')
    {
        $query = $this->model->select('id', 'name', 'description', 'address', 'created_at', 'updated_at')
            ->where('name', 'LIKE', "%$keyword%")
            ->orWhere('address', 'LIKE', "%$keyword%")
            ->orWhere('description', 'LIKE', "%$keyword%");

        if (!$counting) {

            if ($limit > 0) {
                $query->skip($offset)
                    ->take($limit);
            }

            if ($orderBy != null && $orderType != null) {
                $query->orderBy($orderBy, $orderType);
            }
        } else {
            return $query->count();
        }

        return $query->get();
    }

    /**
     * @param $arr
     * @return mixed
     */
    public function edit($arr)
    {
        $company = $this->model->find($arr['id']);

        if ($company != null) {
            $company->fill($arr);

            return $company->save();
        }

        return false;
    }

    /**
     * @param $arr
     * @return bool
     */
    public function store($arr)
    {
        $company = new $this->model;
        $company->fill($arr);

        return $company->save();
    }

    /**
     * @param $id
     * @return bool
     */
    public function deleteById($id): bool
    {
        $company = $this->model->find($id);

        if ($company != null) {
//            $company->roles()->detach();
            return $company->delete();
        }
        return false;
    }

    /**
     * @return array|null
     */
    public function getCompanyList()
    {
        $companyList = $this->model()::select('id', 'name')->orderBy('name')->get();
        return $companyList;
    }
}
