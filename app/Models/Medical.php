<?php

namespace App\Models;

use App\Model\Company;
use Illuminate\Database\Eloquent\Model;
use App\Models\MedicalType;

class Medical extends Model
{
    protected $table = 'medical';

    protected $fillable = [
        'name', 'introduce', 'image', 'thumbnail', 'price', 'quantity_in_repository'
    ];

    public function medical_type()
    {
        return $this->belongsTo(MedicalType::class, 'medical_type_id', 'id');
    }

    public function company()
    {
        return $this->belongsTo(Company::class, 'company_id', 'id');
    }
}
