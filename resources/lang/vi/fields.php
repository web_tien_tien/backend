<?php

return [
    'role_name' => 'Mã nhóm người dùng',
    'role_dislay_name' => 'Tên hiển thị',
    'role_description' => 'Mô tả',
    'role_default_page' => 'Trang mặc định',
    'permission_name' => 'Mã quyền',
    'permission_display_name' => 'Tên hiển thị',
    'permission_description' => 'Mô tả',
    'user_name' => 'Tên đăng nhập',
    'user_display_name' => 'Tên hiển thị',
    'user_email' => 'Email',
    'user_repassword' => 'Mật khẩu nhập lại',
    'user_password' => 'Mật khẩu',
    'medical_type_name' => 'Tên loại thuốc',
    'medical_type_description' => 'Mô tả loại thuốc',
    'sick_type_name' => 'Tên loại bệnh',
    'sick_type_description' => 'Mô tả loại bệnh',
    'company_name' => 'Tên công ty',
    'company_address' => 'Địa chỉ công ty',
    'company_description' => 'Mô tả công ty',
    'sick_name' => 'Tên bệnh',
    'medical_name' => 'Tên thuốc',

];
