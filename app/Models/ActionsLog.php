<?php

namespace App\Models;



use App\User;
use Illuminate\Database\Eloquent\Model;

class ActionsLog extends Model
{
    protected $fillable = ['user_id', 'action_name', 'old_value','new_value','object_name','object_id'];
    protected $table = 'actions_logs';
    public $timestamps = false;

    public function users()
    {
        return $this->belongsTo(User::class);
    }
}
