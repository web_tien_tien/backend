<?php

Route::group(['prefix' => '/user', 'as' => 'user.'], function () {
    Route::post('listing', 'UserController@listing')->name('listing');
    Route::post('store', 'UserController@store')->name('create');
    Route::post('edit', 'UserController@edit')->name('edit');
    Route::post('delete', 'UserController@delete')->name('delete');
    Route::post('update-password','UserController@updatePassword')->name('update-password');
    Route::get('get-role-list','RoleController@getRoleList')->name('get-role-list');
});
