<?php

Route::group(['prefix' => '/medical-type', 'as' => 'medical-type.'], function () {
    Route::post('listing', 'MedicalTypeController@listing')->name('listing');
    Route::post('store', 'MedicalTypeController@store')->name('create');
    Route::post('edit', 'MedicalTypeController@edit')->name('edit');
    Route::post('delete', 'MedicalTypeController@delete')->name('delete');
});
