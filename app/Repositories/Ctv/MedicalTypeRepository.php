<?php

namespace App\Repositories\Ctv;

use App\Models\MedicalType;
use App\Repositories\BaseRepository;

class MedicalTypeRepository extends BaseRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return MedicalType::class;
    }

    /**
     * @param null $keyword
     * @param bool $counting
     * @param int $limit
     * @param int $offset
     * @param string $orderBy
     * @param string $orderType
     * @return mixed
     */
    public function getList($keyword = null, $counting = false, $limit = 10, $offset = 0, $orderBy = 'name', $orderType = 'asc')
    {
        $query = $this->model->select('id', 'name', 'description','created_at','updated_at')
            ->where('name', 'LIKE', "%$keyword%")
            ->orWhere('description', 'LIKE', "%$keyword%");

        if (!$counting) {

            if ($limit > 0) {
                $query->skip($offset)
                    ->take($limit);
            }

            if ($orderBy != null && $orderType != null) {
                $query->orderBy($orderBy, $orderType);
            }
        } else {
            return $query->count();
        }

        return $query->get();
    }

    /**
     * @param $arr
     * @return mixed
     */
    public function edit($arr)
    {
        $medicalType = $this->model->find($arr['id']);

        if ($medicalType != null) {
            $medicalType->fill($arr);

            return $medicalType->save();
        }

        return false;
    }

    /**
     * @param $arr
     * @return bool
     */
    public function store($arr)
    {
        $medicalType = new $this->model;
        $medicalType->fill($arr);

        return $medicalType->save();
    }

    /**
     * @param $id
     * @return bool
     */
    public function deleteById($id): bool
    {
        $medicalType = $this->model->find($id);

        if ($medicalType != null) {
//            $medicalType->roles()->detach();
            return $medicalType->delete();
        }
        return false;
    }

    /**
     * @return array|null
     */
    public function getMedicalTypeList()
    {
        $medicalTypeList = $this->model()::select('id', 'name')->orderBy('name')->get();
        return $medicalTypeList;
    }
}
