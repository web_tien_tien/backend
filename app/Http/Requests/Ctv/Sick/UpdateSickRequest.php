<?php

namespace App\Http\Requests\Ctv\Sick;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateSickRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'id'=>'required|numeric',
            'name' => [
                'required',
                'max:255',
                Rule::unique('sick', 'name')->ignore($this->input('id'))
            ],
        ];
    }

    /**
     * assign attributes
     * @return array
     */
    public function attributes()
    {
        return [
            'name' => trans('fields.sick_name'),
        ];
    }
}
