<?php

namespace App\Http\Controllers\CTV;

use App\Repositories\Ctv\DiseaseTypeRepository;
use App\Http\Requests\Ctv\DiseaseType\DeleteDiseaseTypeRequest;
use App\Http\Requests\Ctv\DiseaseType\StoreDiseaseTypeRequest;
use App\Http\Requests\Ctv\DiseaseType\UpdateDiseaseTypeRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DiseaseTypeController extends Controller
{
    /**
     * @var DiseaseTypeRepository
     */
    protected $diseaseTypeRepository;

    function __construct(DiseaseTypeRepository $diseaseTypeRepository)
    {
        $this->middleware('auth');

        $this->diseaseTypeRepository = $diseaseTypeRepository;
    }

    public function store(StoreDiseaseTypeRequest $request)
    {
        $result = $this->diseaseTypeRepository->store($request->only('name', 'description'));

        return processCommonResponse($result);
    }

    public function edit(UpdateDiseaseTypeRequest $request)
    {
        $result = $this->diseaseTypeRepository->edit($request->only('id', 'name', 'description'));

        return processCommonResponse($result);
    }

    public function delete(DeleteDiseaseTypeRequest $request)
    {
        $result = $this->diseaseTypeRepository->deleteById($request->input('id'));

        return processCommonResponse($result);
    }

    public function listing(Request $request)
    {
        $params = getDataTableRequestParams($request);

        $total = $this->diseaseTypeRepository->getList(
            $params['keyword'],
            true
        );

        $arr = array(
            'recordsTotal' => $total,
            'data' => $this->diseaseTypeRepository->getList(
                $params['keyword'],
                false,
                $params['length'],
                $params['start'],
                $params['orderBy'],
                $params['orderType']
            ),
            'draw' => $params['draw'],
            'recordsFiltered' => $total
        );
        return response()->json($arr);
    }

    /**
     * Get diseaseType list
     * @return \Illuminate\Http\JsonResponse|null
     */
    public function getSickTypeList()
    {
        $diseaseTypeList = $this->diseaseTypeRepository->getSickTypeList();
        return response()->json([
            'code' => CODE_SUCCESS,
            'data' => $diseaseTypeList
        ]);
    }
}
