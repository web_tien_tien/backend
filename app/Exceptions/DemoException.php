<?php

namespace App\Exceptions;

use Exception;
use App\User;
use App\Models\Role;
use App\Notifications\DemoNotification;

class DemoException extends Exception
{
    public function report()
    {
        $role = 'Admin';
        $data = ['message' => 'Lỗi hệ thống'];

//        $admin = User::find('1')->first();
        $roleAdmin = Role::find('1')->first();
        $roleAdmin->notify(new DemoNotification('system_error', $role, $data));
    }

    public function render()
    {

    }
}
