<?php

namespace App\Repositories\Ctv;

use App\Models\DiseaseType;
use App\Repositories\BaseRepository;

class DiseaseTypeRepository extends BaseRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return DiseaseType::class;
    }

    /**
     * @param null $keyword
     * @param bool $counting
     * @param int $limit
     * @param int $offset
     * @param string $orderBy
     * @param string $orderType
     * @return mixed
     */
    public function getList($keyword = null, $counting = false, $limit = 10, $offset = 0, $orderBy = 'name', $orderType = 'asc')
    {
        $query = $this->model->select('id', 'name', 'description','created_at','updated_at')
            ->where('name', 'LIKE', "%$keyword%")
            ->orWhere('description', 'LIKE', "%$keyword%");

        if (!$counting) {

            if ($limit > 0) {
                $query->skip($offset)
                    ->take($limit);
            }

            if ($orderBy != null && $orderType != null) {
                $query->orderBy($orderBy, $orderType);
            }
        } else {
            return $query->count();
        }
        return $query->get();
    }

    /**
     * @param $arr
     * @return mixed
     */
    public function edit($arr)
    {
        $diseaseType = $this->model->find($arr['id']);

        if ($diseaseType != null) {
            $diseaseType->fill($arr);

            return $diseaseType->save();
        }

        return false;
    }

    /**
     * @param $arr
     * @return bool
     */
    public function store($arr)
    {
        $diseaseType = new $this->model;
        $diseaseType->fill($arr);

        return $diseaseType->save();
    }

    /**
     * @param $id
     * @return bool
     */
    public function deleteById($id): bool
    {
        $diseaseType = $this->model->find($id);

        if ($diseaseType != null) {
//            $diseaseType->roles()->detach();
            return $diseaseType->delete();
        }
        return false;
    }

    /**
     * @return array|null
     */
    public function getSickTypeList()
    {
        $diseaseTypeList = $this->model()::select('id', 'name')->orderBy('name')->get();
        return $diseaseTypeList;
    }
}
