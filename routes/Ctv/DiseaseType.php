<?php

Route::group(['prefix' => '/disease-type', 'as' => 'disease-type.'], function () {
    Route::post('listing', 'DiseaseTypeController@listing')->name('listing');
    Route::post('store', 'DiseaseTypeController@store')->name('create');
    Route::post('edit', 'DiseaseTypeController@edit')->name('edit');
    Route::post('delete', 'DiseaseTypeController@delete')->name('delete');
});
