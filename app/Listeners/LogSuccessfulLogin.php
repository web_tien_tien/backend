<?php

namespace App\Listeners;

use App\Events\LoggedIn;
use App\Repositories\Admin\ActionsLogRepository;
use App\Models\ActionsLog;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class LogSuccessfulLogin
{
    protected $actionsLogRepository;

    /**
     * LogSuccessfulLogin constructor.
     * @param ActionsLogRepository $actionsLogRepository
     */
    public function __construct(ActionsLogRepository $actionsLogRepository)
    {
        //
        $this->actionsLogRepository = $actionsLogRepository;
    }

    /**
     * Handle the event.
     *
     * @param  LoggedIn  $event
     * @return void
     */
    public function handle(LoggedIn $event)
    {
        //
        $arr = [
            'user_id' => $event->user->id,
            'action_name' => 'User Login',
        ];
       $this->actionsLogRepository->store($arr);
    }
}
