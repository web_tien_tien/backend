<?php

namespace App\Http\Controllers\CTV;

use App\Http\Controllers\Controller;
use App\Http\Requests\Ctv\Sick\DeleteSickRequest;
use App\Http\Requests\Ctv\Sick\StoreSickRequest;
use App\Http\Requests\Ctv\Sick\UpdateSickRequest;
use App\Repositories\Ctv\SickRepository;
use Illuminate\Http\Request;

class SickController extends Controller
{
    /**
     * @var SickRepository
     */
    protected $sickRepository;

    function __construct(SickRepository $sickRepository)
    {
        $this->middleware('auth');

        $this->sickRepository = $sickRepository;
    }

    public function store(StoreSickRequest $request)
    {
        $result = $this->sickRepository->store($request->only('name', 'introduce', 'sick_type'));

        return processCommonResponse($result);
    }

    public function edit(UpdateSickRequest $request)
    {
        $result = $this->sickRepository->edit($request->only('id', 'name', 'introduce', 'sick_type'));

        return processCommonResponse($result);
    }

    public function delete(DeleteSickRequest $request)
    {
        $result = $this->sickRepository->deleteById($request->input('id'));

        return processCommonResponse($result);
    }

    public function listing(Request $request)
    {
        $params = getDataTableRequestParams($request);

        $total = $this->sickRepository->getList(
            $params['keyword'],
            true
        );

        $arr = array(
            'recordsTotal' => $total,
            'data' => $this->sickRepository->getList(
                $params['keyword'],
                false,
                $params['length'],
                $params['start'],
                $params['orderBy'],
                $params['orderType']
            ),
            'draw' => $params['draw'],
            'recordsFiltered' => $total
        );

        return response()->json($arr);
    }

    /**
     * Get sick list
     * @return \Illuminate\Http\JsonResponse|null
     */
    public function getSickList()
    {
        $sickList = $this->sickRepository->getSickList();
        return response()->json([
            'code' => CODE_SUCCESS,
            'data' => $sickList
        ]);
    }
}
