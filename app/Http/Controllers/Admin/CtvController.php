<?php

namespace App\Http\Controllers\Admin;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;
use Validator;
use Illuminate\Support\Facades\Input;

class CtvController extends BaseController
{
    protected $db_ctv;

    /**
     * tao cong tac vien
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(Request $request){
        $validator = Validator::make($request->all(), [
            'username' => 'required|string|max:255',
            'email' => 'required|email|unique:users',
            'password' => 'required'
        ]);
        if($validator->fails()){
            return $this->send_response($validator->errors(), $this->message_invalid(), 400);
        }
        $ctv = new User();
        $ctv->username = $request['username'];
        $ctv->email = $request['email'];
        $ctv->password = bcrypt($request['password']);
        $ctv->role = 1;
        $ctv->timestamps;
        if(!$ctv->save()){
            //neu co conflict xay ra khong tao duoc nguoi dung
            return $this->send_response([], $this->message_conflict(), 409);
        }
        return $this->send_response($ctv, $this->message_action_success(), 200);
    }

    /**
     * lay ra danh sach cong tac vien
     * @return \Illuminate\Http\JsonResponse
     */
    public function get_list(){
        $params = Input::all();
        $validator = Validator::make($params, [
            'limit'=> 'required|numeric',
            'page' => 'required|numeric'
        ]);
        if($validator->fails()){
            return $this->send_response($validator->errors(), $this->message_invalid(), 400);
        }
        $list_ctv = User::where('status', 1)
                        ->where('role', 1);
        //neu khong co ctv nao
        if($list_ctv->count() == 0){
            return $this->send_response([], $this->message_data_success(), 200);
        }
        $list_ctv_send = $list_ctv->paginate($params['limit']);
        return $this->send_response($list_ctv_send, $this->message_data_success(), 200);
    }

    /**
     * lay thong tin chi tiet ve mot ctv
     * @param $user_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function detail($user_id){
        try{
            $ctv = User::where('role', 1)->where('id', $user_id)->first();
            if(is_null($ctv)){
                return $this->send_response([], $this->message_data_not_found(), 404);
            }
            return $this->send_response($ctv, $this->message_data_success(), 200);
        }catch (\Exception $ex){
            return $this->send_response([], $this->message_server(), 500);
        }
    }

    /**
     * lay ra danh sach cong tac vien
     * @param $user_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete($user_id){
        try{
            $ctv = User::where('role', 1)->where('id', $user_id)->first();
            if(is_null($ctv)){
                return $this->send_response([], $this->message_data_not_found(), 404);
            }
            $result = $ctv->update([
                'status' => 2,
                'updated_at' => date('Y-m-d H:i:s')
            ]);
            if($result == 1){
                return $this->send_response([], $this->message_action_success(), 200);
            }
            return $this->send_response([], $this->message_conflict(), 409);
        }catch (\Exception $ex){
            return $this->send_response([], $this->message_server(), 500);
        }

    }

    /**
     * lay lai mat khau cho ctv
     * @param Request $request
     * @param $user_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function reset_password(Request $request, $user_id){
        try{
            $ctv = User::where('role', 1)->where('id', $user_id)->first();
            if(is_null($ctv)){
                return $this->send_response([], $this->message_data_not_found(), 404);
            }
            $validator = Validator::make($request->all(), [
                'password' => 'required'
            ]);
            if($validator->fails()){
                return $this->send_response($validator->errors(), $this->message_invalid(), 400);
            }
            $result = $ctv->update([
                'password' =>bcrypt($request['password']),
                'updated_at' => date('Y-m-d H:i:s')
            ]);
            if($result){
                return $this->send_response([], $this->message_action_success(), 200);
            }
            return $this->send_response([], $this->message_error(), 409);

        }catch (\Exception $ex){
            return $this->send_response([], $this->message_server(), 500);
        }
    }
}
