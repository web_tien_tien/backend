<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Database\QueryException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use App\Exceptions\DemoException;
use App\Models\Role;
use App\Notifications\DemoNotification;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        if ($exception instanceof \ErrorException) {
            $role = 'Admin';
            $data = [
//                'message' => $exception->getMessage()
                'message' => $exception->getMessage()
            ];

            $roleAdmin = Role::find('1')->first();
            $roleAdmin->notify(new DemoNotification('system_error', $role, $data));
//            dd($exception);
        } elseif ($exception instanceof QueryException) {
            $role = 'Admin';
            $data = [
                'message' => $exception->getMessage()
            ];

            $roleAdmin = Role::find('1')->first();
            $roleAdmin->notify(new DemoNotification('system_error', $role, $data));
//            dd($exception);
        } elseif ($exception instanceof \PDOException) {

            $role = 'Admin';
            $data = [
                'message' => $exception->getMessage()
            ];

            $roleAdmin = Role::find('1')->first();
            $roleAdmin->notify(new DemoNotification('system_error', $role, $data));
//            dd($exception);
        } elseif ($exception instanceof Exception) {
            $role = 'Admin';
            $data = [
                'message' => $exception->getMessage()
            ];

            $roleAdmin = Role::find('1')->first();
            $roleAdmin->notify(new DemoNotification('system_error', $role, $data));
//            dd($exception);
        }


        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Exception $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        return parent::render($request, $exception);
    }
}
