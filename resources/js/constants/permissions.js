//Quản lý hệ thống
export const DASHBOARD_ACCESS = "dashboard.access";
export const LOG_ACCESS = "log.access";

//Quản lý người dùng
export const PERMISSION_MANAGEMENT = "permission.management";
export const ROLE_MANAGEMENT = "role.management";
export const USER_MANAGEMENT = "user.management";


//Quyền cộng tác viên
export const CAN_ORDER = "canOrder";
export const MEDICAL_TYPE_MANAGEMENT = "medical_type.management";
export const MEDICAL_MANAGEMENT = "medical.management";
export const SICK_TYPE_MANAGEMENT = "sick_type.management";
export const SICK_MANAGEMENT = "sick.management";
export const COMPANY_MANAGEMENT = "company.management";

//Quyền người dùng thông thường
export const STANDARD_MANAGEMENT = "standard.management";