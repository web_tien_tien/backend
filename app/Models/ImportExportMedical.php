<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ImportExportMedical extends Model
{
    protected $fillable = ['medical_id', 'price', 'type','quantity'];
    protected $table = 'import_export_medical';

}
