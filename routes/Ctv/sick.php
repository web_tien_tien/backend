<?php

Route::group(['prefix' => '/sick', 'as' => 'sick.'], function () {
    Route::post('listing', 'SickController@listing')->name('listing');
    Route::post('store', 'SickController@store')->name('create');
    Route::post('edit', 'SickController@edit')->name('edit');
    Route::post('delete', 'SickController@delete')->name('delete');
    Route::get('get-sick-type-list', 'DiseaseTypeController@getSickTypeList')->name('get-sick-type-list');

});
