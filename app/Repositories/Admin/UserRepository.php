<?php

namespace App\Repositories\Admin;

use App\User;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\Hash;

class UserRepository extends BaseRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return User::class;
    }

    /**
     * @param null $keyword
     * @param bool $counting
     * @param int $limit
     * @param int $offset
     * @param string $orderBy
     * @param string $orderType
     * @return mixed
     */
    public function getList($keyword = null, $counting = false, $limit = 10, $offset = 0, $orderBy = 'name', $orderType = 'asc')
    {
        $query = $this->model->with("roles:roles.id,name")
            ->select('id', 'name', 'display_name', 'email', 'created_at', 'updated_at')
            ->where('name', 'LIKE', "%$keyword%")
            ->orWhere('email', 'LIKE', "%$keyword%");

        if (!$counting) {

            if ($limit > 0) {
                $query->skip($offset)
                    ->take($limit);
            }

            if ($orderBy != null && $orderType != null) {
                $query->orderBy($orderBy, $orderType);
            }
        } else {
            return $query->count();
        }

        return $query->get();
    }

    /**
     * @param $arr
     * @return mixed
     */
    public function edit($arr)
    {
        $role = $arr['role'];
        $user = $this->model->find($arr['id']);
        if ($user != null) {
            $user->fill($arr);
            if ($user->save()) {
                $user->roles()->detach();
                if ($role != null) {
                    $user->attachRole($role['id']);
                }
                return true;
            }
            return false;
        }
        return false;
    }

    /**
     * @param $arr
     * @return bool
     */
    public function store($arr)
    {
        $role = $arr['role'];
        $user = new $this->model;
        $user->fill($arr);
        $user->password = Hash::make($arr['password']);
        if ($user->save()) {
            if ($role != null) {
                $user->attachRole($role['id']);
            }
            return true;
        }
        return false;
    }

    /**
     * @param $id
     * @return bool
     */
    public function deleteById($id): bool
    {

        $user = $this->model->find($id);

        if ($user != null) {
            $user->roles()->detach();
            return $this->model->where('id', $id)->delete();
        }
        return false;
    }

    public function updatePassword($arr)
    {
        $user = $this->model->find($arr['id']);
        if ($user != null) {
            $user->password = Hash::make($arr['password']);
            return $user->save();
        }
        return false;
    }

    public function updateProfile($arr)
    {
        $user = $this->model->find($arr['id']);
        if ($user != null) {
            $user->fill($arr);
            if ($user->save()) {
                return true;
            }
        }
        return false;
    }
}
