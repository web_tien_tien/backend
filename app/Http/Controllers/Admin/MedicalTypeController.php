<?php
//
//namespace App\Http\Controllers\Admin;
//
//use App\Repositories\Admin\MedicalTypeRepository;
//use App\Http\Requests\Admin\MedicalType\DeleteMedicalTypeRequest;
//use App\Http\Requests\Admin\MedicalType\StoreMedicalTypeRequest;
//use App\Http\Requests\Admin\MedicalType\UpdateMedicalTypeRequest;
//use Illuminate\Http\Request;
//use App\Http\Controllers\Controller;
//
//class MedicalTypeController extends Controller
//{
//    /**
//     * @var MedicalTypeRepository
//     */
//    protected $medicalTypeRepository;
//
//    function __construct(MedicalTypeRepository $medicalTypeRepository)
//    {
//        $this->middleware('auth');
//
//        $this->medicalTypeRepository = $medicalTypeRepository;
//    }
//
//    public function store(StoreMedicalTypeRequest $request)
//    {
//        $result = $this->medicalTypeRepository->store($request->only('name', 'description'));
//
//        return processCommonResponse($result);
//    }
//
//    public function edit(UpdateMedicalTypeRequest $request)
//    {
//        $result = $this->medicalTypeRepository->edit($request->only('id', 'name', 'description'));
//
//        return processCommonResponse($result);
//    }
//
//    public function delete(DeleteMedicalTypeRequest $request)
//    {
//        $result = $this->medicalTypeRepository->deleteById($request->input('id'));
//
//        return processCommonResponse($result);
//    }
//
//    public function listing(Request $request)
//    {
//        $params = getDataTableRequestParams($request);
//
//        $total = $this->medicalTypeRepository->getList(
//            $params['keyword'],
//            true
//        );
//
//        $arr = array(
//            'recordsTotal' => $total,
//            'data' => $this->medicalTypeRepository->getList(
//                $params['keyword'],
//                false,
//                $params['length'],
//                $params['start'],
//                $params['orderBy'],
//                $params['orderType']
//            ),
//            'draw' => $params['draw'],
//            'recordsFiltered' => $total
//        );
//
//        return response()->json($arr);
//    }
//
//    /**
//     * Get medicalType list
//     * @return \Illuminate\Http\JsonResponse|null
//     */
//    public function getMedicalTypeList()
//    {
//        $medicalTypeList = $this->medicalTypeRepository->getMedicalTypeList();
//        return response()->json([
//            'code' => CODE_SUCCESS,
//            'data' => $medicalTypeList
//        ]);
//    }
//}
