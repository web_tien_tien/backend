<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Model\Medical;

class Company extends Model
{
    protected $table = 'company';

    protected $fillable = [
        'name', 'address', 'description'
    ];

    public function medicals()
    {
        return $this->hasMany(Medical::class, 'company_id', 'id');
    }
}
