<?php

Route::group(['prefix' => '/company', 'as' => 'company.'], function () {
    Route::post('listing', 'CompanyController@listing')->name('listing');
    Route::post('store', 'CompanyController@store')->name('create');
    Route::post('edit', 'CompanyController@edit')->name('edit');
    Route::post('delete', 'CompanyController@delete')->name('delete');
});
