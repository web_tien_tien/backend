<?php

namespace App\Repositories\Admin;

use App\Models\Permission;
use App\Repositories\BaseRepository;

class PermissionRepository extends BaseRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return Permission::class;
    }

    /**
     * @param null $keyword
     * @param bool $counting
     * @param int $limit
     * @param int $offset
     * @param string $orderBy
     * @param string $orderType
     * @return mixed
     */
    public function getList($keyword = null, $counting = false, $limit = 10, $offset = 0, $orderBy = 'name', $orderType = 'asc')
    {
        $query = $this->model->select('id', 'name', 'display_name', 'description')
            ->where('name', 'LIKE', "%$keyword%")
            ->orWhere('display_name', 'LIKE', "%$keyword%");

        if (!$counting) {

            if ($limit > 0) {
                $query->skip($offset)
                    ->take($limit);
            }

            if ($orderBy != null && $orderType != null) {
                $query->orderBy($orderBy, $orderType);
            }
        } else {
            return $query->count();
        }

        return $query->get();
    }

    /**
     * @param $arr
     * @return mixed
     */
    public function edit($arr)
    {
        $permission = $this->model->find($arr['id']);

        if ($permission != null) {
            $permission->fill($arr);

            return $permission->save();
        }

        return false;
    }

    /**
     * @param $arr
     * @return bool
     */
    public function store($arr)
    {
        $permission = new $this->model;
        $permission->fill($arr);

        return $permission->save();
    }

    /**
     * @param $id
     * @return bool
     */
    public function deleteById($id): bool
    {
        $permission = $this->model->find($id);

        if ($permission != null) {
            $permission->roles()->detach();
            return $permission->delete();
        }
        return false;
    }

    /**
     * @return array|null
     */
    public function getPermissionList()
    {
        $permissionList = $this->model()::select('id', 'display_name', 'name')->orderBy('display_name')->get();
        return $permissionList;
    }
}
