<?php

if (!function_exists('includeRouteFiles')) {

    /**
     * Loops through a folder and requires all PHP files
     * Searches sub-directories as well.
     *
     * @param $folder
     */
    function includeRouteFiles($folder)
    {
        try {
            $rdi = new recursiveDirectoryIterator($folder);
            $it = new recursiveIteratorIterator($rdi);

            while ($it->valid()) {
                if (!$it->isDot() && $it->isFile() && $it->isReadable() && $it->current()->getExtension() === 'php') {
                    require $it->key();
                }

                $it->next();
            }
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }
}

/**
 * get base dataTable request params
 * @param $request
 * @return array
 */
function getDataTableRequestParams($request){
    $start = $request->input('start');
    $length = $request->input('length');
    $draw = $request->input('draw');

    $order = $request->input('order');
    $columns = $request->input('columns');

    $num = $order[0]['column'];
    $orderBy = $columns[$num]['data'];
    $orderType = $order[0]['dir'];

    $search = $request->input('search');
    $keyword = $search['value'];

    return [
        'start' => $start,
        'length' => $length,
        'draw' => $draw,
        'orderBy' => $orderBy,
        'orderType' => $orderType,
        'keyword' => $keyword
    ];
}

/**
 * @param $result
 * @param null $data
 * @return \Illuminate\Http\JsonResponse
 */
function processCommonResponse($result, $data = null)
{
    return response()->json(array(
        'code' => $result ? CODE_SUCCESS : CODE_ERROR,
        'message' => $result ? MESSAGE_SUCCESS : MESSAGE_ERROR,
        'data' => $data
    ));
}
