<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Medical;

class MedicalType extends Model
{
    protected $table = 'medical_type';

    protected $fillable = [
        'name', 'description'
    ];

    public function medicals()
    {
        return $this->hasMany(Medical::class, 'medical_type_id', 'id');
    }
}
