<?php

namespace App\Repositories\General;

use App\Models\Notification;
use App\User;
use App\Repositories\BaseRepository;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;


class NotificationRepository extends BaseRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return Notification::class;
    }

    public function listing($count = false, $roleName, $typeNotification = null, $page = 1)
    {
        $query = DB::table('notifications')
            ->orderBy('created_at', 'desc')
            ->where(function ($query) use ($roleName, $typeNotification) {
                $query->where('data->typeNotification', $typeNotification);
                $query->where('data->role', $roleName);
            });


        if ($count == true) {
            $now = Carbon::now()->format('Y-m-d H:i:s');
            $twoHoursAgo = Carbon::now()->subHours(2)->format('Y-m-d H:i:s');
            return $query->whereBetween('created_at', [$twoHoursAgo, $now])->count();
        } else {
            return $query
                ->select(['id', 'data', 'created_at', 'user_read'])
                ->skip(10 * ($page - 1))
                ->take(10)
                ->get();
        }


    }

}
