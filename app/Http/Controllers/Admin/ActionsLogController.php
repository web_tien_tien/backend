<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repositories\Admin\ActionsLogRepository;
use Illuminate\Http\Request;

class ActionsLogController extends Controller
{
    /**
     * @var ActionsLogRepository
     */
    protected $actionsLogRepository;

    function __construct(ActionsLogRepository $actionsLogRepository)
    {
        $this->middleware('auth');

        $this->actionsLogRepository = $actionsLogRepository;
    }

    public function listing(Request $request)
    {
        $params = getDataTableRequestParams($request);

        $total = $this->actionsLogRepository->getList(
            $params['keyword'],
            true
        );

        $arr = array(
            'recordsTotal' => $total,
            'data' => $this->actionsLogRepository->getList(
                $params['keyword'],
                false,
                $params['length'],
                $params['start'],
                $params['orderBy'],
                $params['orderType']
            ),
            'draw' => $params['draw'],
            'recordsFiltered' => $total
        );

        return response()->json($arr);
    }
}
