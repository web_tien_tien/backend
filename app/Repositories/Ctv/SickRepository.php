<?php

namespace App\Repositories\Ctv;

use App\Models\Sick;
use App\Repositories\BaseRepository;

class SickRepository extends BaseRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return Sick::class;
    }

    /**
     * @param null $keyword
     * @param bool $counting
     * @param int $limit
     * @param int $offset
     * @param string $orderBy
     * @param string $orderType
     * @return mixed
     */
    public function getList($keyword = null, $counting = false, $limit = 10, $offset = 0, $orderBy = 'name', $orderType = 'asc')
    {
        $query = $this->model->select('id', 'name', 'sick_type_id', 'introduce', 'updated_at')
            ->with('sick_type:sick_type.id,name')
            ->where('name', 'LIKE', "%$keyword%")
            ->orWhere('introduce', 'LIKE', "%$keyword%");

        if (!$counting) {

            if ($limit > 0) {
                $query->skip($offset)
                    ->take($limit);
            }

            if ($orderBy != null && $orderType != null) {
                $query->orderBy($orderBy, $orderType);
            }
        } else {
            return $query->count();
        }

        return $query->get();
    }

    /**
     * @param $arr
     * @return mixed
     */
    public function edit($arr)
    {
        $sick_type_id = $arr['sick_type']['id'];
        $sick = $this->model->find($arr['id']);
        $sick->fill($arr);
        $sick->sick_type_id = $sick_type_id;
        return $sick->save();
    }

    /**
     * @param $arr
     * @return bool
     */
    public function store($arr)
    {
        $sick_type_id = $arr['sick_type']['id'];
        $sick = new $this->model;
        $sick->fill($arr);
        $sick->sick_type_id = $sick_type_id;
        return $sick->save();
    }

    /**
     * @param $id
     * @return bool
     */
    public function deleteById($id): bool
    {

        $sick = $this->model->find($id);

        if ($sick != null) {
            return $this->model->where('id', $id)->delete();
        }
        return false;
    }

    public function getSickList()
    {
        $sickList = $this->model()::select('id', 'display_name')->orderBy('display_name')->get();
        return $sickList;
    }
}
