import routes, { adminRoutes, ctvRoutes } from '~/router/routes'

export default [
    {
        title: 'menu.dashboard',
        icon: 'flaticon-line-graph',
        route: routes.dashboard
    },
    {
        title: 'menu.user_manager',
        icon: 'flaticon-user',
        children: [
            {
                title: 'menu.admin.user',
                route: adminRoutes.user
            },
            {
                title: 'menu.admin.role',
                route: adminRoutes.role
            },
            {
                title: 'menu.admin.permission',
                route: adminRoutes.permission
            }
        ]
    },
    {
        title: 'menu.medical_manager',
        icon: 'fa fa-briefcase-medical',
        children: [
            {
                title: 'menu.ctv.medical',
                route: ctvRoutes.medical
            },
            {
                title: 'menu.ctv.medical_type',
                route: ctvRoutes.medical_type
            },
            {
                title:'menu.ctv.company',
                route: ctvRoutes.company
            }
        ]
    },
    {
        title: 'menu.disease_manager',
        icon: 'fa fa-file-medical',
        children: [
            {
                title: 'menu.ctv.disease_type',
                route: ctvRoutes.disease_type
            },
            {
                title: 'menu.ctv.sick',
                route: ctvRoutes.sick
            },
        ]
    },
    {
        title: 'menu.admin.log',
        icon: 'flaticon-edit-1',
        route: adminRoutes.log
    },
]
