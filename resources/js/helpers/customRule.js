import {PASSWORD_REGEX, USER_NAME_REGEX} from '../constants/regex'
import VeeValidate, {Validator} from 'vee-validate';


 export function customRule() {
    VeeValidate.Validator.extend('isPassword', {
            getMessage: '',
            validate: function (value) {
                return PASSWORD_REGEX.test(value)
            },
        }
    );
    VeeValidate.Validator.extend('isUsername', {
            getMessage: '',
            validate: function (value) {
                return USER_NAME_REGEX.test(value)
            },
        }
    );
    const dictionary = {
        vi: {
            messages: {
                isPassword: 'Mật khẩu phải bao gồm chữ hoa, chữ thường,số, kí tự đặc biệt và tối thiểu 8 kí tự',
                isUsername: 'Tên người dùng chỉ sử dụng chữ cái không dấu hoặc chữ số và không được bắt đầu bằng chữ số'
            }
        },
        en: {
            messages: {
                isPassword: 'Passwords must include uppercase, lowercase, numeric, special characters, and a minimum of 8 characters',
                isUsername: 'User names are only alphanumeric or numeric and may not begin with a digit'
            }
        }
    }

    VeeValidate.Validator.localize(dictionary)
}
