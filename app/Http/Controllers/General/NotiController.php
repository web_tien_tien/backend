<?php

namespace App\Http\Controllers\General;

use App\Models\Notification;
use App\Repositories\General\NotificationRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Notifications\sentNotification;
use Illuminate\Support\Facades\Auth;
use App\User;

class NotiController extends Controller
{
    /**
     * @var NotificationRepository
     */
    protected $notificationRepository;

    function __construct(NotificationRepository $notificationRepository)
    {
        $this->middleware('auth');

        $this->notificationRepository = $notificationRepository;
    }

    public function listing(Request $request)
    {
        $user = Auth::user()->roles()->get();
        $roleName = $user[0]['name'];

        $page = $request->input('page');
        $typeNotification = $request->input('typeNotification');
        $data['notifications'] = $this->notificationRepository->listing(false, $roleName, $typeNotification, $page);
        $data['count'] = $this->notificationRepository->listing(true, $roleName,$typeNotification);
        return response()->json($data);
    }

    public function read(Request $request)
    {
        $notificationId = $request->input('notificationId');
        $newReadAt = $request->input('newReadAt');
        $notification = Notification::find($notificationId);
        $notification->user_read = $newReadAt;

        return processCommonResponse($notification->save());
    }
}
