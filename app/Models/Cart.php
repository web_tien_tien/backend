<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Model\DetailCart;

class Cart extends Model
{
    protected $table = 'cart';

    protected $fillable = [
        'id', 'status', 'user_id', 'created_at', 'updated_at'
    ];

    public function detail_carts(){
        return $this->hasMany(DetailCart::class, 'cart_id', 'id');
    }
}
