<?php
header('Access-Control-Allow-Origin:  *');
header('Access-Control-Allow-Methods:  POST, GET, OPTIONS, PUT, DELETE');
header('Access-Control-Allow-Headers:  Content-Type, X-Auth-Token, Origin, Authorization');
http_response_code(201);

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Tungtn112

Route::post('auth/login', 'AuthController@login')->name('login');

Route::group([
    'middleware' => ['api', 'auth:api']
], function () {

    includeRouteFiles(__DIR__ . '/Auth/');

    Route::group(['namespace' => 'Admin', 'prefix' => '/admin', 'as' => 'admin.', 'middleware' => ['role:CTV|Admin']], function () {
        includeRouteFiles(__DIR__ . '/Admin/');
    });

    Route::group(['namespace'=>'CTV','prefix'=>'/ctv','as'=>'ctv.','middleware'=>[]],function (){
        includeRouteFiles(__DIR__ . '/Ctv/');
    });

    Route::group(['namespace' => 'General', 'prefix' => '/general','as' => 'general.','middleware' =>[]],function (){
        includeRouteFiles(__DIR__.'/General/');
    });
});

//API của TinBK

//Route::post('register', 'Auth\RegisterController@register_account');
Route::post('login', 'Auth\LoginController@login');

//Route::get('image_book/name={image_name}', [
//    'uses' => 'System\ManageImage@get_image_book',
//    'as' => 'book.image'
//]);
//
//Route::get('image_contest/name={image_name}', [
//    'uses' => 'System\ManageImage@get_image_contest',
//    'as' => 'contest.image'
//]);

Route::group(['middleware' => ['api', 'auth:api']], function () {
    //logoout
//    Route::post('logout', 'Auth\LoginController@logout');

    //admin
    Route::group(['prefix' => 'admin', 'middleware' => ['role:CTV']], function () {
        //cong tac vien
        Route::group(['prefix' => 'ctv'], function () {
            //create cong tac vien
            Route::post('/create', 'Admin\CtvController@create');

            //lay ra danh sach cong tac vien ( cong tac vien con hoat dong) status = 1
            Route::get('/list', 'Admin\CtvController@get_list');

            //lay thong tin chi tiet cua ctv
            Route::get('/detail={user_id}', 'Admin\CtvController@detail');

            //lay lai mat khau cho ctv
            Route::post('/reset_password={user_id}', 'Admin\CtvController@reset_password');

            //xoa mot cong tac vien ( status = 2)
            Route::post('/delete={user_id}', 'Admin\CtvController@delete');
        });
    });

    //cong tac vien
    Route::group(['middleware' => ['role:CTV']], function () {
        Route::group(['prefix' => 'ctv'], function () {
            //medical
            Route::group(['prefix' => 'medical'], function () {
                //create
                Route::post('/create', 'CTV\MedicalController@create');

                //update
                Route::post('/update={medical_id}', 'CTV\MedicalController@update');

                //lay ra danh sach thuoc phan trang
                Route::get('/list', 'CTV\MedicalController@list_medical');

                //xem chi tiet mot thuoc
                Route::get('/detail={medical_id}', 'CTV\MedicalController@detail');


            });

            //medical type
            Route::group(['prefix' => 'medical_type'], function () {
                //create
                Route::post('/create', 'CTV\MedicalTypeController@create');

                //update
                Route::post('/update={medical_type_id}', 'CTV\MedicalTypeController@update');

                //lay ra danh sach nhom thuoc co phan trang
                Route::get('/list', 'CTV\MedicalTypeController@list_medical_type');

                //lay ra thong tin chi tiet mot nhom thuoc
                Route::get('/detail={medical_type}', 'CTV\MedicalTypeController@detail');

                //lay ra danh sach thuoc trong 1 nhom thuoc
                Route::get('/list_medical/medical_type={medical_type_id}', 'CTV\MedicalTypeController@list_medical');

            });

            //company
            Route::group(['prefix' => 'company'], function () {
                //create
                Route::post('/create', 'CTV\CompanyController@create');

                //update
                Route::post('/update={company_id}', 'CTV\CompanyController@update');

                //lay ra danh sach cong ty phan trang
                Route::get('/list', 'CTV\CompanyController@list_company');

                //lay ra thong tin chi tiet cua
                Route::get('/detail={company_id}', 'CTV\CompanyController@detail');

                //lay ra danh sach thuoc cua cong ty nay
                Route::get('/list_medicals/company={company_id}', 'CTV\CompanyController@list_medicals');

            });
        });
    });

});

