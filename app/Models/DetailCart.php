<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Model\Medical;

class DetailCart extends Model
{
    protected $table = 'cart_detail';
    protected $fillable = [
        'id', 'status', 'cart_id', 'medical_id', 'created_at', 'updated_at'
    ];

    public function medical(){
        return $this->hasOne(Medical::class, 'medical_id', 'id');
    }
}
