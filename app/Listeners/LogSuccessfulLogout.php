<?php

namespace App\Listeners;

use App\Events\LoggedOut;
use App\Models\ActionsLog;
use App\Repositories\Admin\ActionsLogRepository;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class LogSuccessfulLogout
{
    protected $actionsLogRepository;

    /**
     * LogSuccessfulLogout constructor.
     * @param ActionsLogRepository $actionsLogRepository
     */
    public function __construct(ActionsLogRepository $actionsLogRepository)
    {
        //
        $this->actionsLogRepository = $actionsLogRepository;
    }

    /**
     * Handle the event.
     *
     * @param  LoggedOut  $event
     * @return void
     */
    public function handle(LoggedOut $event)
    {
        //
        $arr = [
            'user_id' => $event->user->id,
            'action_name' => 'User Logout',
        ];
        $this->actionsLogRepository->store($arr);
    }
}
