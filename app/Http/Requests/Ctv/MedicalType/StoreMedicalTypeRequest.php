<?php

namespace App\Http\Requests\Ctv\MedicalType;

use Illuminate\Foundation\Http\FormRequest;

class StoreMedicalTypeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:255|unique:medical_type,name',
            'description' => 'max:255'
        ];
    }

    /**
     * assign attributes
     * @return array
     */
    public function attributes()
    {
        return [
            'name' => trans('fields.medical_type_name'),
            'description' => trans('fields.medical_type_description')
        ];
    }
}
