<?php

namespace App\Http\Controllers\CTV;

use App\Models\MedicalType;
use App\Service\SlugString;
use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

use App\Repositories\Ctv\MedicalTypeRepository;
use App\Http\Requests\Ctv\MedicalType\DeleteMedicalTypeRequest;
use App\Http\Requests\Ctv\MedicalType\StoreMedicalTypeRequest;
use App\Http\Requests\Ctv\MedicalType\UpdateMedicalTypeRequest;

class MedicalTypeController extends BaseController
{
    //Tungnt112
    /**
     * @var MedicalTypeRepository
     */
    protected $medicalTypeRepository;

    function __construct(MedicalTypeRepository $medicalTypeRepository)
    {
        $this->middleware('auth');

        $this->medicalTypeRepository = $medicalTypeRepository;
    }

    public function store(StoreMedicalTypeRequest $request)
    {
        $result = $this->medicalTypeRepository->store($request->only('name', 'description'));

        return processCommonResponse($result);
    }

    public function edit(UpdateMedicalTypeRequest $request)
    {
        $result = $this->medicalTypeRepository->edit($request->only('id', 'name', 'description'));

        return processCommonResponse($result);
    }

    public function delete(DeleteMedicalTypeRequest $request)
    {
        $result = $this->medicalTypeRepository->deleteById($request->input('id'));

        return processCommonResponse($result);
    }

    public function listing(Request $request)
    {
        $params = getDataTableRequestParams($request);

        $total = $this->medicalTypeRepository->getList(
            $params['keyword'],
            true
        );

        $arr = array(
            'recordsTotal' => $total,
            'data' => $this->medicalTypeRepository->getList(
                $params['keyword'],
                false,
                $params['length'],
                $params['start'],
                $params['orderBy'],
                $params['orderType']
            ),
            'draw' => $params['draw'],
            'recordsFiltered' => $total
        );

        return response()->json($arr);
    }

    /**
     * Get medicalType list
     * @return \Illuminate\Http\JsonResponse|null
     */
    public function getMedicalTypeList()
    {
        $medicalTypeList = $this->medicalTypeRepository->getMedicalTypeList();
        return response()->json([
            'code' => CODE_SUCCESS,
            'data' => $medicalTypeList
        ]);
    }







    //Tin BK
    /**
     * tao them nhom thuoc moi
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(Request $request){
        try{
            $validator = Validator::make($request->all(), [
                'name' => 'required|string|max:255'
            ]);
            if($validator->fails()){
                return $this->send_response($validator->errors(), $this->message_invalid(), 400);
            }
            $medical_type = new MedicalType();
            $medical_type->name = $request['name'];
            $medical_type->slug = SlugString::to_slug($request['name']);
            $medical_type->timestamps;
            if(!$medical_type->save()){
                return $this->send_response([], $this->message_conflict(), 409);
            }
            return $this->send_response($medical_type, $this->message_action_success(), 200);
        }catch (\Exception $ex){
            return $this->send_response([], $this->message_server(), 500);
        }
    }

    /**
     * update thong tin nhom thuoc
     * @param Request $request
     * @param $medical_type_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $medical_type_id){
        try{
            $validator = Validator::make($request->all(), [
                'name' => 'required|string|max:255'
            ]);
            if($validator->fails()){
                return $this->send_response($validator->errors(), $this->message_invalid(), 400);
            }
            $medical_type = MedicalType::find($medical_type_id);
            if(is_null($medical_type)){
                return $this->send_response([], $this->message_data_not_found(), 404);
            }
            $result = $medical_type->update([
                'name' => $request['name'],
                'slug' => SlugString::to_slug($request['name']),
                'updated_at' => date('Y-m-d H:i:s')
            ]);
            if($result){
                return $this->send_response($medical_type, $this->message_action_success(), 200);
            }
            return $this->send_response($medical_type, $this->message_action_success(), 200);
        }catch (\Exception $ex){
            return $this->send_response([], $this->message_server(), 500);
        }
    }

    /**
     * lay danh sach nhom thuoc co phan trang
     * @return \Illuminate\Http\JsonResponse
     */
    public function list_medical_type(){
        try{
            $params = Input::all();
            $validator = Validator::make($params, [
                'limit' => 'required|numeric',
                'page' => 'required|numeric'
            ]);
            if($validator->fails()){
                return $this->send_response($validator->errors(), $this->message_invalid(), 400);
            }
            $medical_type = MedicalType::all()->forPage($params['page'], $params['limit']);
            return $this->send_response($medical_type, $this->message_data_success(), 200);
        }catch (\Exception $ex){
            return $this->send_response([], $this->message_server(), 500);
        }
    }

    /**
     * lay ra loai thuoc chi tiet
     * @param $medical_type_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function detail($medical_type_id){
        try{
            $medical_type = MedicalType::find($medical_type_id);
            if(is_null($medical_type)){
                return $this->send_response([], $this->message_data_not_found(), 404);
            }
            return $this->send_response($medical_type, $this->message_data_success(), 200);
        }catch (\Exception $ex){
            return $this->send_response([], $this->message_server(), 500);
        }
    }

    /**
     * lay ra danh sach thuoc co trong nhom thuoc do
     * @param $medical_type_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function list_medical($medical_type_id){
        try{
            $params = Input::all();
            $validator = Validator::make($params, [
                'limit' => 'required|numeric',
                'page' => 'required|numeric'
            ]);
            if($validator->fails()){
                return $this->send_response($validator->errors(), $this->message_invalid(), 400);
            }
            $medical_type = MedicalType::find($medical_type_id);
            if(is_null($medical_type)){
                return $this->send_response([], $this->message_data_not_found(), 404);
            }
            $medicals = $medical_type->medicals()->forPage($params['page'], $params['limit']);
            return $this->send_response($medicals, $this->message_data_success(), 200);
        }catch (\Exception $ex){
            return $this->send_response([], $this->message_server(), 500);
        }
    }
}
