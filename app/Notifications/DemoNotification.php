<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;

class DemoNotification extends Notification implements ShouldQueue
{
    use Queueable;

    private $data;

    private $type;

    private $role;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($type, $role, $data)
    {
        $this->data = $data;
        $this->type = $type;
        $this->role = $role;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database', 'broadcast'];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'typeNotification' => $this->type,
            'role' => $this->role,
            'data' => $this->data
        ];
    }
}
