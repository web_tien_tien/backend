<?php

namespace App\Http\Controllers\CTV;

use App\Models\Medical;
use App\Service\SlugString;
use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Ramsey\Uuid\Uuid;

use App\Http\Requests\Ctv\Medical\DeleteMedicalRequest;
use App\Http\Requests\Ctv\Medical\StoreMedicalRequest;
use App\Http\Requests\Ctv\Medical\UpdateMedicalRequest;
use App\Repositories\Ctv\MedicalRepository;

use Intervention\Image\Facades\Image;

class MedicalController extends BaseController
{
//    Tungnt112

    /**
     * @var MedicalRepository
     */
    protected $medicalRepository;

    function __construct(MedicalRepository $medicalRepository)
    {
        $this->middleware('auth');

        $this->medicalRepository = $medicalRepository;
    }

    public function store(StoreMedicalRequest $request)
    {
        if($request->file('image')->isValid()) {
            $image = $request->image;

            $arr = $request->only('name', 'introduce', 'medical_type_id', 'company_id','price','quantity_in_repository');
            $newFileName = time() . $image->getClientOriginalName();
            $image->storeAs('public/medicals/images', $newFileName);
            $imagePath = '/storage/medicals/images/' . $newFileName;
            $arr['image'] = $imagePath;
            Image::make(storage_path('app/public/medicals/images/' . $newFileName))->resize(370, 250)->save(storage_path('app/public/medicals/images/thumb_' . $newFileName));
            $thumbnail = '/storage/medicals/images/thumb_' . $newFileName;
            $arr['thumbnail'] = $thumbnail;

            $result = $this->medicalRepository->store($arr);
            return processCommonResponse($result);
        }

        return processCommonResponse(false);
    }

    public function edit(Request $request)
    {
        if($request->hasfile('image') ) {
            if ($request->file('image')->isValid()) {
                $arr = $request->only('id', 'name', 'introduce', 'medical_type_id', 'company_id','price','quantity_in_repository');

                // find and delete old image from storage
                $medical = Medical::find($arr['id']);
                $oldImage = 'app/public' . substr($medical->image, 8);
                $oldThumbnail = 'app/public' . substr($medical->thumbnail, 8);
                if (file_exists(storage_path($oldImage)))
                    unlink(storage_path($oldImage));
                if (file_exists(storage_path($oldThumbnail)))
                    unlink(storage_path($oldThumbnail));

                //them anh moi
                $image = $request->image;

                $newFileName = time() . $image->getClientOriginalName();
                $image->storeAs('public/medicals/images', $newFileName);
                $imagePath = '/storage/medicals/images/' . $newFileName;
                $arr['image'] = $imagePath;
                Image::make(storage_path('app/public/medicals/images/' . $newFileName))->resize(370, 250)->save(storage_path('app/public/medicals/images/thumb_' . $newFileName));
                $thumbnail = '/storage/medicals/images/thumb_' . $newFileName;
                $arr['thumbnail'] = $thumbnail;

                $result = $this->medicalRepository->edit($arr);
                return processCommonResponse($result);
            }

            return processCommonResponse(false);
        }
        else{
            $arr = $request->only('id', 'name', 'introduce', 'medical_type_id', 'company_id','price','quantity_in_repository');
            $result = $this->medicalRepository->edit($arr);
            return processCommonResponse($result);
        }
    }

    public function delete(DeleteMedicalRequest $request)
    {
        $id = $request->input('id');
        $medical = Medical::find($id);
        $oldImage = 'app/public' . substr($medical->image, 8);
        $oldThumbnail = 'app/public' . substr($medical->thumbnail, 8);
        unlink(storage_path($oldImage));
        unlink(storage_path($oldThumbnail));

        $result = $this->medicalRepository->deleteById($request->input('id'));

        return processCommonResponse($result);
    }

    public function listing(Request $request)
    {
        $params = getDataTableRequestParams($request);

        $total = $this->medicalRepository->getList(
            $params['keyword'],
            true
        );

        $arr = array(
            'recordsTotal' => $total,
            'data' => $this->medicalRepository->getList(
                $params['keyword'],
                false,
                $params['length'],
                $params['start'],
                $params['orderBy'],
                $params['orderType']
            ),
            'draw' => $params['draw'],
            'recordsFiltered' => $total
        );

        return response()->json($arr);
    }

    /**
     * Get medical list
     * @return \Illuminate\Http\JsonResponse|null
     */
    public function getMedicalList()
    {
        $medicalList = $this->medicalRepository->getMedicalList();
        return response()->json([
            'code' => CODE_SUCCESS,
            'data' => $medicalList
        ]);
    }





    //Tin BK

    /**
     * tao thuoc moi
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'name' => 'required|string|max:255',
                'code' => 'required|string',
                'medical_type_id' => 'required|numeric',
                'company_id' => 'required|numeric',
                'introduce' => 'required',
                'image' => 'required|image|mimes:jpeg, png, bmp, gif, svg, jpg',
                'price' => 'required|numeric',
                'sale' => 'required|numeric',
            ]);
            if ($validator->fails()) {
                return $this->send_response($validator->errors(), $this->message_invalid(), 400);
            }
            $image = $request->file('image');
            $image_name = Uuid::uuid4() . "." . $image->getClientOriginalExtension();
            $medical = new Medical();
            $medical->name = $request['name'];
            $medical->code = $request['code'];
            $medical->medical_type_id = $request['medical_type_id'];
            $medical->company_id = $request['company_id'];
            $medical->introduce = $request['introduce'];
            $medical->image = $image_name;
            $medical->price = $request['price'];
            $medical->sale = $request['sale'];
            $medical->slug = SlugString::to_slug($request['name']);
            $medical->timestamps;

            if (!$medical->save()) {
                return $this->send_response([], $this->message_conflict(), 409);
            }
            Storage::disk('medical_image')->put($image_name, File::get($image));
            return $this->send_response($medical, $this->message_action_success(), 200);
        } catch (\Exception $ex) {
            return $this->send_response([], $this->message_server(), 500);
        }
    }

    /**
     * update thong tin thuoc
     * @param Request $request
     * @param $medical_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $medical_id)
    {
        try {
            $validator = Validator::make($request->all(), [
                'name' => 'required|string|max:255',
                'code' => 'required|string',
                'medical_type_id' => 'required|numeric',
                'company_id' => 'required|numeric',
                'introduce' => 'required',
                'image' => 'required|image|mimes:jpeg, png, bmp, gif, svg, jpg',
                'price' => 'required|numeric',
                'sale' => 'required|numeric',
            ]);
            if ($validator->fails()) {
                return $this->send_response($validator->errors(), $this->message_invalid(), 400);
            }
            $image = $request->file('image');
            $medical = Medical::find($medical_id);
            if (is_null($medical)) {
                return $this->send_response([], $this->message_data_not_found(), 404);
            }
            $image_name = $medical->image;
            $result = $medical->update([
                'name' => $request['name'],
                'code' => $request['code'],
                'medical_type_id' => $request['medical_type_id'],
                'company_id' => $request['company_id'],
                'introduce' => $request['introduce'],
                'price' => $request['price'],
                'sale' => $request['sale'],
                'slug' => SlugString::to_slug($request['name']),
                'updated_at' => date('Y-m-d H:i:s')
            ]);
            if ($result) {
                Storage::disk('medical_image')->put($image_name, File::get($image));
                return $this->send_response($medical, $this->message_action_success(), 200);
            }

            return $this->send_response([], $this->message_error(), 409);

        } catch (\Exception $ex) {
            return $this->send_response([], $this->message_server(), 500);
        }
    }

    /**
     * lay ra danh sach thuoc co phan trang
     * @return \Illuminate\Http\JsonResponse
     */
    public function list_medical()
    {
        try {
            $params = Input::all();
            $validator = Validator::make($params, [
                'limit' => 'required|numeric',
                'page' => 'required|numeric'
            ]);
            if ($validator->fails()) {
                return $this->send_response($validator->errors(), $this->message_invalid(), 400);
            }
            $medical = Medical::all()->forPage($params['page'], $params['limit']);
            return $this->send_response($medical, $this->message_data_success(), 200);
        } catch (\Exception $ex) {
            return $this->send_response([], $this->message_server(), 500);
        }
    }

    /**
     * lay ra thong tin chi tiet ve mot thuoc
     * @param $medical_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function detail($medical_id)
    {
        try {
            $medical = Medical::where('medical.id', $medical_id)
                ->join('medical_type', 'medical.medical_type_id', '=', 'medical_type.id')
                ->join('company', 'medical.company_id', '=', 'medical.company_id')
                ->select('medical.*', 'medical_type.name as medical_type_name', 'company.name as company_name')
                ->first();
            if (is_null($medical)) {
                return $this->send_response([], $this->message_data_not_found(), 404);
            }
            return $this->send_response($medical, $this->message_data_success(), 200);
        } catch (\Exception $ex) {
            return $this->send_response($ex, $this->message_server(), 500);
        }
    }

}
