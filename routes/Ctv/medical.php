<?php

Route::group(['prefix' => '/medical', 'as' => 'medical.'], function () {
    Route::post('listing', 'MedicalController@listing')->name('listing');
    Route::post('store', 'MedicalController@store')->name('create');
    Route::post('edit', 'MedicalController@edit')->name('edit');
    Route::post('delete', 'MedicalController@delete')->name('delete');
    Route::get('get-medical-type-list', 'MedicalTypeController@getMedicalTypeList')->name('get-medical-type-list');
    Route::get('get-company-list', 'CompanyController@getCompanyList')->name('get-company-list');


});
