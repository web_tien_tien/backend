<?php

namespace App\Http\Requests\Ctv\Medical;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateMedicalRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'id'=>'required|numeric',
            'name' => [
                'required',
                'max:255',
                Rule::unique('medical', 'name')->ignore($this->input('id'))
            ],
            'image' =>'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'price' => 'numeric|required',
            'quantity_in_repository'=>'numeric|required'
        ];
    }

    /**
     * assign attributes
     * @return array
     */
    public function attributes()
    {
        return [
            'name' => trans('fields.medical_name'),
        ];
    }
}
