<?php

namespace App\Http\Requests\Admin\Company;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateCompanyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'id'=>'required|numeric',
            'name' => [
                'required',
                'max:255',
                Rule::unique('medical_type', 'name')->ignore($this->input('id'))
            ],
            'description' => 'max:255',
            'address' => 'max:255|required'
        ];
    }

    /**
     * assign attributes
     * @return array
     */
    public function attributes()
    {
        return [
            'name' => trans('fields.company_name'),
            'description' => trans('fields.company_description'),
            'address'=>trans('fields.company_address')
        ];
    }
}
