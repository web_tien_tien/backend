<?php

Route::group(['prefix' => '/user', 'as' => 'user.'], function () {
    Route::post('update-profile','UpdateProfileController@updateProfile')->name('update-profile');
});
