<?php

namespace App\Http\Controllers\General;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Admin\UserRepository;

class UpdateProfileController extends Controller
{

    /**
     * @var UserRepository
     */
    protected $userRepository;

    function __construct(UserRepository $userRepository)
    {
        $this->middleware('auth');

        $this->userRepository = $userRepository;
    }

    public function updateProfile(Request $request)
    {
        $result = $this->userRepository->updateProfile($request->only('id', 'display_name', 'phone', 'address'));
        return processCommonResponse($result);
    }
}
