<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Model\Sick;

class TypeSick extends Model
{
    protected $table = 'sick_type';

    protected $fillable = [
        'id', 'status', 'name', 'created_at', 'updated_at', 'slug'
    ];

    public function sicks(){
        return $this->hasMany(Sick::class, 'type_sick_id', 'id');
    }
}
