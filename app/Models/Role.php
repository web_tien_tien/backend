<?php

namespace App\Models;

use Zizaco\Entrust\EntrustRole;
use Illuminate\Notifications\Notifiable;

class Role extends EntrustRole
{
    use Notifiable;

    protected $fillable = ['name', 'display_name', 'description'];
}
