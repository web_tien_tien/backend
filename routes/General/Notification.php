<?php

Route::group(['prefix' => '/notification', 'as' => 'notification.'], function () {
    Route::post('/listing','NotiController@listing')->name('listing');
    Route::post('/read','NotiController@read')->name('read');
});
