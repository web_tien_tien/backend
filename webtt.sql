-- MySQL dump 10.13  Distrib 8.0.13, for macos10.14 (x86_64)
--
-- Host: localhost    Database: webtt
-- ------------------------------------------------------
-- Server version	8.0.13

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `actions_logs`
--

DROP TABLE IF EXISTS `actions_logs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `actions_logs` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL COMMENT 'Người thực hiện',
  `action_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Tên hành động',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `object_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Lớp đối tượng thay đổi',
  `object_id` int(10) unsigned DEFAULT NULL COMMENT 'Đối tượng thay đổi',
  `old_value` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci COMMENT 'Trạng thái trước',
  `new_value` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci COMMENT 'Trạng thái sau',
  PRIMARY KEY (`id`),
  KEY `actions_logs_user_id_foreign_key_idx` (`user_id`),
  CONSTRAINT `actions_logs_user_id_foreign_key` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `actions_logs`
--

LOCK TABLES `actions_logs` WRITE;
/*!40000 ALTER TABLE `actions_logs` DISABLE KEYS */;
INSERT INTO `actions_logs` VALUES (1,1,'User Login','2018-11-07 01:47:45',NULL,NULL,NULL,NULL),(2,1,'User Logout','2018-11-07 02:01:13',NULL,NULL,NULL,NULL),(3,1,'User Login','2018-11-07 02:01:25',NULL,NULL,NULL,NULL),(4,1,'User Logout','2018-11-07 06:56:54',NULL,NULL,NULL,NULL),(5,1,'User Login','2018-11-07 07:27:54',NULL,NULL,NULL,NULL),(6,1,'User Logout','2018-11-07 07:27:59',NULL,NULL,NULL,NULL),(7,1,'User Login','2018-11-07 07:28:47',NULL,NULL,NULL,NULL),(8,1,'User Logout','2018-11-07 07:28:50',NULL,NULL,NULL,NULL),(9,1,'User Login','2018-11-07 07:34:29',NULL,NULL,NULL,NULL),(10,1,'User Logout','2018-11-07 07:43:26',NULL,NULL,NULL,NULL),(11,1,'User Login','2018-11-07 08:45:33',NULL,NULL,NULL,NULL),(12,1,'User Login','2018-11-09 01:26:00',NULL,NULL,NULL,NULL),(13,1,'User Login','2018-11-13 07:05:04',NULL,NULL,NULL,NULL),(14,1,'User Login','2018-11-13 08:30:35',NULL,NULL,NULL,NULL),(15,1,'User Login','2018-11-14 01:25:39',NULL,NULL,NULL,NULL),(16,1,'User Logout','2018-11-14 01:49:40',NULL,NULL,NULL,NULL),(17,1,'User Logout','2018-11-14 01:49:41',NULL,NULL,NULL,NULL),(18,1,'User Login','2018-11-14 01:49:47',NULL,NULL,NULL,NULL),(19,1,'User Logout','2018-11-14 01:50:39',NULL,NULL,NULL,NULL),(20,1,'User Login','2018-11-14 01:50:48',NULL,NULL,NULL,NULL),(21,1,'User Logout','2018-11-14 06:46:20',NULL,NULL,NULL,NULL),(22,1,'User Login','2018-11-14 06:49:07',NULL,NULL,NULL,NULL),(23,1,'User Login','2018-11-14 06:49:22',NULL,NULL,NULL,NULL),(24,1,'User Login','2018-11-14 06:49:59',NULL,NULL,NULL,NULL),(25,1,'User Login','2018-11-14 06:50:40',NULL,NULL,NULL,NULL),(26,1,'User Login','2018-11-14 06:51:12',NULL,NULL,NULL,NULL),(27,1,'User Login','2018-11-14 06:51:39',NULL,NULL,NULL,NULL),(28,1,'User Login','2018-11-14 06:55:11',NULL,NULL,NULL,NULL),(29,1,'User Logout','2018-11-14 07:07:55',NULL,NULL,NULL,NULL),(30,1,'User Login','2018-11-14 07:08:00',NULL,NULL,NULL,NULL),(31,1,'User Login','2018-11-14 07:11:13',NULL,NULL,NULL,NULL),(32,1,'User Login','2018-11-14 07:11:38',NULL,NULL,NULL,NULL),(33,1,'User Login','2018-11-14 07:12:56',NULL,NULL,NULL,NULL),(34,1,'User Login','2018-11-14 07:15:09',NULL,NULL,NULL,NULL),(35,4,'User Login','2018-11-14 07:17:10',NULL,NULL,NULL,NULL),(36,1,'User Login','2018-11-14 07:30:40',NULL,NULL,NULL,NULL),(37,1,'User Login','2018-11-14 07:41:53',NULL,NULL,NULL,NULL),(38,1,'User Login','2018-11-14 08:46:55',NULL,NULL,NULL,NULL),(39,1,'User Login','2018-11-14 16:03:39',NULL,NULL,NULL,NULL),(40,4,'User Login','2018-11-14 16:08:06',NULL,NULL,NULL,NULL),(41,4,'User Login','2018-11-14 16:18:30',NULL,NULL,NULL,NULL),(42,4,'User Login','2018-11-14 16:23:42',NULL,NULL,NULL,NULL),(43,5,'User Login','2018-11-14 16:59:52',NULL,NULL,NULL,NULL),(44,5,'User Logout','2018-11-14 17:35:07',NULL,NULL,NULL,NULL),(45,5,'User Login','2018-11-14 17:35:14',NULL,NULL,NULL,NULL),(46,5,'User Logout','2018-11-14 17:55:45',NULL,NULL,NULL,NULL),(47,12,'User Login','2018-11-14 17:55:53',NULL,NULL,NULL,NULL),(48,12,'User Logout','2018-11-14 18:02:59',NULL,NULL,NULL,NULL),(49,5,'User Login','2018-11-14 18:03:04',NULL,NULL,NULL,NULL),(50,5,'User Logout','2018-11-14 18:06:14',NULL,NULL,NULL,NULL),(51,12,'User Login','2018-11-14 18:06:20',NULL,NULL,NULL,NULL),(52,12,'User Logout','2018-11-14 18:08:56',NULL,NULL,NULL,NULL),(53,5,'User Login','2018-11-14 18:09:02',NULL,NULL,NULL,NULL),(54,5,'User Logout','2018-11-14 18:16:17',NULL,NULL,NULL,NULL),(55,12,'User Login','2018-11-14 18:16:23',NULL,NULL,NULL,NULL),(56,12,'User Login','2018-11-14 18:30:17',NULL,NULL,NULL,NULL),(57,12,'User Login','2018-11-14 18:46:27',NULL,NULL,NULL,NULL),(58,5,'User Login','2018-11-14 18:59:44',NULL,NULL,NULL,NULL),(59,5,'User Login','2018-11-14 19:07:28',NULL,NULL,NULL,NULL),(60,5,'User Login','2018-11-14 19:30:03',NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `actions_logs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cart`
--

DROP TABLE IF EXISTS `cart`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `cart` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='gio hang';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cart`
--

LOCK TABLES `cart` WRITE;
/*!40000 ALTER TABLE `cart` DISABLE KEYS */;
/*!40000 ALTER TABLE `cart` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cart_detail`
--

DROP TABLE IF EXISTS `cart_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `cart_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` int(11) DEFAULT '1',
  `cart_id` int(11) NOT NULL,
  `medical_id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='chi tiet gio hang';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cart_detail`
--

LOCK TABLES `cart_detail` WRITE;
/*!40000 ALTER TABLE `cart_detail` DISABLE KEYS */;
/*!40000 ALTER TABLE `cart_detail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `company`
--

DROP TABLE IF EXISTS `company`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `company` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` int(11) DEFAULT '1',
  `code` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='cong ty';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `company`
--

LOCK TABLES `company` WRITE;
/*!40000 ALTER TABLE `company` DISABLE KEYS */;
/*!40000 ALTER TABLE `company` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `medical`
--

DROP TABLE IF EXISTS `medical`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `medical` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` int(11) DEFAULT '1',
  `code` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `medical_type_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `introduce` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `price` double DEFAULT NULL,
  `sale` double DEFAULT NULL,
  `views` int(11) DEFAULT '0',
  `quantity_sold` int(11) DEFAULT '0',
  `quantity_in_repository` int(11) DEFAULT '10',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='thuoc';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `medical`
--

LOCK TABLES `medical` WRITE;
/*!40000 ALTER TABLE `medical` DISABLE KEYS */;
/*!40000 ALTER TABLE `medical` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `medical_type`
--

DROP TABLE IF EXISTS `medical_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `medical_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` int(11) DEFAULT '1',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='nhom thuoc';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `medical_type`
--

LOCK TABLES `medical_type` WRITE;
/*!40000 ALTER TABLE `medical_type` DISABLE KEYS */;
/*!40000 ALTER TABLE `medical_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2018_11_01_100227_entrust_setup_tables',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `new_table`
--

DROP TABLE IF EXISTS `new_table`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `new_table` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `display_name` varchar(128) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `new_table`
--

LOCK TABLES `new_table` WRITE;
/*!40000 ALTER TABLE `new_table` DISABLE KEYS */;
/*!40000 ALTER TABLE `new_table` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `notifications`
--

DROP TABLE IF EXISTS `notifications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `notifications` (
  `id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `notifiable_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `notifiable_id` bigint(20) unsigned NOT NULL,
  `data` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `read_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notifications`
--

LOCK TABLES `notifications` WRITE;
/*!40000 ALTER TABLE `notifications` DISABLE KEYS */;
/*!40000 ALTER TABLE `notifications` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order`
--

DROP TABLE IF EXISTS `order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` int(11) DEFAULT '1',
  `user_id` int(11) NOT NULL,
  `total_cost` double NOT NULL,
  `username` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `note` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='don hang';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order`
--

LOCK TABLES `order` WRITE;
/*!40000 ALTER TABLE `order` DISABLE KEYS */;
/*!40000 ALTER TABLE `order` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_detail`
--

DROP TABLE IF EXISTS `order_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `order_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` int(11) DEFAULT '1',
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `number` int(11) NOT NULL,
  `price` double NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='chi tiet don hang';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_detail`
--

LOCK TABLES `order_detail` WRITE;
/*!40000 ALTER TABLE `order_detail` DISABLE KEYS */;
/*!40000 ALTER TABLE `order_detail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `password_resets` (
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permission_role`
--

DROP TABLE IF EXISTS `permission_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `permission_role` (
  `permission_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  KEY `permission_role_role_id_foreign` (`role_id`),
  CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permission_role`
--

LOCK TABLES `permission_role` WRITE;
/*!40000 ALTER TABLE `permission_role` DISABLE KEYS */;
INSERT INTO `permission_role` VALUES (40,18),(42,19),(41,19),(1,1),(2,1),(3,1),(4,1),(5,1);
/*!40000 ALTER TABLE `permission_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `permissions_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permissions`
--

LOCK TABLES `permissions` WRITE;
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
INSERT INTO `permissions` VALUES (1,'permission.management','Quản lý quyền người dùng','Hiển thị danh sách; thêm, sửa, xóa quyền người dùng','2018-11-01 21:01:28','2018-11-01 21:01:28'),(2,'role.management','Quản lý nhóm người dùng','Hiển thị danh sách; thêm, sửa, xóa nhóm người dùng','2018-11-01 21:01:43','2018-11-01 21:01:43'),(3,'user.management','Quản lý người dùng','Hiển thị danh sách; thêm, sửa, xóa người dùng','2018-11-01 21:01:43','2018-11-01 21:01:43'),(4,'dashboard.access','Truy cập màn hình dashboard','Truy cập màn hình dashboard','2018-11-04 18:37:28','2018-11-05 01:13:58'),(5,'log.access','Truy cập màn hình Log','Truy cập màn hình Log','2018-11-04 19:03:21','2018-11-04 19:03:21'),(40,'ctv','Cộng tác viên','Cộng tác viên','2018-11-10 01:06:39','2018-11-10 01:06:39'),(41,'canOrder','Có thể đặt hàng','Có thể đặt hàng','2018-11-10 01:07:49','2018-11-10 01:07:49'),(42,'standand.user','Người dùng thông thường','Người dùng thông thường','2018-11-10 01:09:34','2018-11-10 01:09:34');
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_in`
--

DROP TABLE IF EXISTS `product_in`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `product_in` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` int(11) DEFAULT NULL,
  `json_product` json NOT NULL,
  `user_id` int(11) NOT NULL,
  `total_cost` double NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='nhap san pham';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_in`
--

LOCK TABLES `product_in` WRITE;
/*!40000 ALTER TABLE `product_in` DISABLE KEYS */;
/*!40000 ALTER TABLE `product_in` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_out`
--

DROP TABLE IF EXISTS `product_out`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `product_out` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` int(11) DEFAULT '1',
  `json_product` json NOT NULL,
  `user_id` int(11) NOT NULL,
  `total_cost` double NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='xuat hang';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_out`
--

LOCK TABLES `product_out` WRITE;
/*!40000 ALTER TABLE `product_out` DISABLE KEYS */;
/*!40000 ALTER TABLE `product_out` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_user`
--

DROP TABLE IF EXISTS `role_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `role_user` (
  `user_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`),
  KEY `role_user_role_id_foreign` (`role_id`),
  CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_user`
--

LOCK TABLES `role_user` WRITE;
/*!40000 ALTER TABLE `role_user` DISABLE KEYS */;
INSERT INTO `role_user` VALUES (5,1),(10,18),(12,18),(11,19);
/*!40000 ALTER TABLE `role_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'Admin','Quản trị hệ thống','Admin',NULL,'2018-11-10 01:11:59'),(18,'CTV','Cộng tác viên','Cộng tác viên','2018-11-10 01:10:23','2018-11-10 01:10:23'),(19,'Stardard User','Người dùng thông thường','Người dùng thông thường','2018-11-10 01:10:58','2018-11-10 01:10:58');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sick`
--

DROP TABLE IF EXISTS `sick`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `sick` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` int(11) DEFAULT '1',
  `code` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `sick_type_id` int(11) NOT NULL,
  `image` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `json_medical` json DEFAULT NULL,
  `introduce` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='benh';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sick`
--

LOCK TABLES `sick` WRITE;
/*!40000 ALTER TABLE `sick` DISABLE KEYS */;
/*!40000 ALTER TABLE `sick` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sick_type`
--

DROP TABLE IF EXISTS `sick_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `sick_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` int(11) DEFAULT '1',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='nhom benh';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sick_type`
--

LOCK TABLES `sick_type` WRITE;
/*!40000 ALTER TABLE `sick_type` DISABLE KEYS */;
/*!40000 ALTER TABLE `sick_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `roles` int(11) DEFAULT '0',
  `status` int(11) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (5,'admin','admin@gmail.com','$2y$10$QH4VGMI8bq.DqToAnCCFcOzpSeVu0AeZRkWcbqxFHGr7bX2hGZUcS','admin',NULL,NULL,NULL,0,1,'2018-11-08 04:04:17','2018-11-08 04:04:17'),(10,'ctv','ctv@gmail.com','$2y$10$.sEKDn7uS3q9n90KGGgKNei.h7wSDhEmG5D.xqy5ZOWIlqusbHGvG','cộng tác viên',NULL,NULL,NULL,0,1,'2018-11-10 01:11:42','2018-11-10 01:11:42'),(11,'user1','user1@gmail.com','$2y$10$k6VKFaSpsbvEZ2rwlnXQX.236RBkpd213E8Iwozo2cTaHkooRR2/q','user1',NULL,NULL,NULL,0,1,'2018-11-10 01:12:54','2018-11-10 01:12:54'),(12,'admin2','admin2@gmail.com','$2y$10$Qrgz7Qkac.sg7Q.SahAmx.n9nlKfSJOXAg9G0MJKtov15rZSMR.uG','admin2',NULL,NULL,NULL,0,1,'2018-11-14 10:49:27','2018-11-14 10:49:27');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-11-15  2:34:48
