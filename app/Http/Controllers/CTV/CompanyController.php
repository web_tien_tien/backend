<?php

namespace App\Http\Controllers\CTV;

use App\Model\Company;
use App\Service\SlugString;
use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

use App\Repositories\Ctv\CompanyRepository;
use App\Http\Requests\Admin\Company\DeleteCompanyRequest;
use App\Http\Requests\Admin\Company\StoreCompanyRequest;
use App\Http\Requests\Admin\Company\UpdateCompanyRequest;

class CompanyController extends BaseController
{
    //Tungnt112
    /**
     * @var CompanyRepository
     */
    protected $companyRepository;

    function __construct(CompanyRepository $companyRepository)
    {
        $this->middleware('auth');

        $this->companyRepository = $companyRepository;
    }

    public function store(StoreCompanyRequest $request)
    {
        $result = $this->companyRepository->store($request->only('name', 'description', 'address'));

        return processCommonResponse($result);
    }

    public function edit(UpdateCompanyRequest $request)
    {
        $result = $this->companyRepository->edit($request->only('id', 'name', 'address', 'description'));

        return processCommonResponse($result);
    }

    public function delete(DeleteCompanyRequest $request)
    {
        $result = $this->companyRepository->deleteById($request->input('id'));

        return processCommonResponse($result);
    }

    public function listing(Request $request)
    {
        $params = getDataTableRequestParams($request);

        $total = $this->companyRepository->getList(
            $params['keyword'],
            true
        );

        $arr = array(
            'recordsTotal' => $total,
            'data' => $this->companyRepository->getList(
                $params['keyword'],
                false,
                $params['length'],
                $params['start'],
                $params['orderBy'],
                $params['orderType']
            ),
            'draw' => $params['draw'],
            'recordsFiltered' => $total
        );

        return response()->json($arr);
    }

    /**
     * Get company list
     * @return \Illuminate\Http\JsonResponse|null
     */
    public function getCompanyList()
    {
        $companyList = $this->companyRepository->getCompanyList();
        return response()->json([
            'code' => CODE_SUCCESS,
            'data' => $companyList
        ]);
    }





    //TinBk

    /**
     * tao cong ty
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'name' => 'required|string|max:255',
                'code' => 'required|string|max:255',
                'address' => 'required|string|max:255'
            ]);
            if ($validator->fails()) {
                return $this->send_response($validator->errors(), $this->message_invalid(), 400);
            }
            $company = new Company();
            $company->name = $request['name'];
            $company->code = $request['code'];
            $company->address = $request['address'];
            $company->slug = SlugString::to_slug($request['name']);
            $company->timestamps;
            if (!$company->save()) {
                return $this->send_response([], $this->message_conflict(), 409);
            }
            return $this->send_response($company, $this->message_action_success(), 200);
        } catch (\Exception $ex) {
            return $this->send_response([], $this->message_server(), 500);
        }
    }

    /**
     * cap nhat thong tin cong ty
     * @param Request $request
     * @param $company_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $company_id)
    {
        try {
            $validator = Validator::make($request->all(), [
                'name' => 'required|string|max:255',
                'code' => 'required|string|max:255',
                'address' => 'required|string|max:255'
            ]);
            if ($validator->fails()) {
                return $this->send_response($validator->errors(), $this->message_invalid(), 400);
            }
            $company = Company::find($company_id);
            if (is_null($company)) {
                return $this->send_response([], $this->message_data_not_found(), 404);
            }
            $result = $company->update([
                'name' => $request['name'],
                'code' => $request['code'],
                'address' => $request['address'],
                'slug' => SlugString::to_slug($request['name']),
                'updated_at' => date('Y-m-d H:i:s')
            ]);
            if ($result) {
                return $this->send_response($company, $this->message_action_success(), 200);
            }
            return $this->send_response($company, $this->message_action_success(), 200);
        } catch (\Exception $ex) {
            return $this->send_response([], $this->message_server(), 500);
        }
    }

    /**
     * lay ra danh sach cty co phan trang
     * @return \Illuminate\Http\JsonResponse
     */
    public function list_company()
    {
        try {
            $params = Input::all();
            $validator = Validator::make($params, [
                'limit' => 'required|numeric',
                'page' => 'required|numeric'
            ]);
            if ($validator->fails()) {
                return $this->send_response($validator->errors(), $this->message_invalid(), 400);
            }
            $companies = Company::all()->forPage($params['page'], $params['limit']);
            return $this->send_response($companies, $this->message_data_success(), 200);
        } catch (\Exception $ex) {
            return $this->send_response([], $this->message_server(), 500);
        }
    }

    /**
     * lay ra thong tin chi tiet ve mot cong ty
     * @param $company_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function detail($company_id)
    {
        try {
            $company = Company::find($company_id);
            if (is_null($company)) {
                return $this->send_response([], $this->message_data_not_found(), 404);
            }
            return $this->send_response($company, $this->message_data_success(), 200);
        } catch (\Exception $ex) {
            return $this->send_response([], $this->message_server(), 500);
        }
    }

    /**
     * xem danh sach thuoc cua cty
     * @param $company_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function list_medicals($company_id)
    {
        try {
            $params = Input::all();
            $validator = Validator::make($params, [
                'limit' => 'required|numeric',
                'page' => 'required|numeric'
            ]);
            if ($validator->fails()) {
                return $this->send_response($validator->errors(), $this->message_invalid(), 400);
            }
            $company = Company::find($company_id);
            if (is_null($company)) {
                return $this->send_response([], $this->message_data_not_found(), 404);
            }
            $medicals = $company->medicals()->forPage($params['page'], $params['limit']);
            return $this->send_response($medicals, $this->message_data_success(), 200);
        } catch (\Exception $ex) {
            return $this->send_response([], $this->message_server(), 500);
        }
    }


}
