<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BaseController extends Controller
{
    /**
     * send response success
     * @return \Illuminate\Http\JsonResponse
     */
    public function send_response($data, $message, $status)
    {
        $response = [
            'status' => $status,
            'data' => $data,
            'message' => $message
        ];

        return response()->json($response, 200);
    }

    public function message_error()
    {
        $message_error = [
            'message' => 'Thực hiện hành động thất bại',
            'code' => 'action_failure'
        ];
        return $message_error;
    }

    public function message_data_not_found(){
        $message_error = [
            'message' => 'Lấy dữ liệu thất bại',
            'code' => 'data_not_found'
        ];
        return $message_error;
    }

    public function message_invalid()
    {
        $message_invalid = [
            'message' => 'Đầu vào không hợp lệ',
            'code' => 'input_invalid'
        ];
        return $message_invalid;
    }

    public function message_data_success()
    {
        $message_data_success = [
            'message' => 'Lấy dữ liệu thành công',
            'code' => 'data_success'
        ];
        return $message_data_success;
    }

    public function message_server()
    {
        $message_server = [
            'message' => 'Xảy ra lỗi trên server',
            'code' => 'server_failure'
        ];
        return $message_server;
    }

    public function message_action_success()
    {
        $message_action_success = [
            'message' => 'Thực hiện hành động thành công',
            'code' => 'action_success'
        ];
        return $message_action_success;
    }

    public function message_error_db(){
        $message_error_db = [
            'message' => 'Lỗi cơ sở dữ liệu',
            'code' => 'database_failure'
        ];
        return $message_error_db;
    }

    public function message_conflict(){
        $message_conflict = [
            'message' => 'Thêm thất bại',
            'code' => 'create_failure'
        ];
        return $message_conflict;
    }
}
