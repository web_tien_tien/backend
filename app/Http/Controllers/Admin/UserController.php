<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests\Admin\User\DeleteAdminRequest;
use App\Http\Requests\Admin\User\UpdateAdminRequest;
use App\Http\Requests\Admin\User\StoreAdminRequest;
use App\Http\Requests\Admin\User\UpdatePasswordRequest;
use App\Repositories\Admin\UserRepository;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    /**
     * @var UserRepository
     */
    protected $userRepository;

    function __construct(UserRepository $userRepository)
    {
        $this->middleware('auth');

        $this->userRepository = $userRepository;
    }

    public function store(StoreAdminRequest $request)
    {
        $result = $this->userRepository->store($request->only('name', 'display_name', 'email', 'password', 'role'));

        return processCommonResponse($result);
    }

    public function edit(UpdateAdminRequest $request)
    {
        $result = $this->userRepository->edit($request->only('id', 'name', 'display_name', 'description', 'role'));

        return processCommonResponse($result);
    }

    public function delete(DeleteAdminRequest $request)
    {
        $result = $this->userRepository->deleteById($request->input('id'));

        return processCommonResponse($result);
    }

    public function updatePassword(UpdatePasswordRequest $request)
    {
        $result = $this->userRepository->updatePassword($request->only('id', 'password'));
        return processCommonResponse($result);
    }

    public function listing(Request $request)
    {
        $params = getDataTableRequestParams($request);

        $total = $this->userRepository->getList(
            $params['keyword'],
            true
        );

        $arr = array(
            'recordsTotal' => $total,
            'data' => $this->userRepository->getList(
                $params['keyword'],
                false,
                $params['length'],
                $params['start'],
                $params['orderBy'],
                $params['orderType']
            ),
            'draw' => $params['draw'],
            'recordsFiltered' => $total
        );

        return response()->json($arr);
    }
}
