<?php

namespace App\Repositories\Ctv;

use App\Models\Medical;
use App\Repositories\BaseRepository;

class MedicalRepository extends BaseRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return Medical::class;
    }

    /**
     * @param null $keyword
     * @param bool $counting
     * @param int $limit
     * @param int $offset
     * @param string $orderBy
     * @param string $orderType
     * @return mixed
     */
    public function getList($keyword = null, $counting = false, $limit = 10, $offset = 0, $orderBy = 'name', $orderType = 'asc')
    {
        $query = $this->model->select('id', 'name', 'medical_type_id', 'company_id', 'introduce','image','thumbnail', 'price', 'quantity_in_repository', 'updated_at')
            ->with('medical_type:medical_type.id,name')
            ->with('company:company.id,name')
            ->where('name', 'LIKE', "%$keyword%")
            ->orWhere('introduce', 'LIKE', "%$keyword%");

        if (!$counting) {

            if ($limit > 0) {
                $query->skip($offset)
                    ->take($limit);
            }

            if ($orderBy != null && $orderType != null) {
                $query->orderBy($orderBy, $orderType);
            }
        } else {
            return $query->count();
        }

        return $query->get();
    }

    /**
     * @param $arr
     * @return mixed
     */
    public function edit($arr)
    {
        $medical_type_id = $arr['medical_type_id'];
        $company_id=$arr['company_id'];
        $medical = $this->model->find($arr['id']);
        $medical->fill($arr);
        $medical->medical_type_id = $medical_type_id;
        $medical->company_id = $company_id;
        return $medical->save();
    }

    /**
     * @param $arr
     * @return bool
     */
    public function store($arr)
    {
        $medical_type_id = $arr['medical_type_id'];
        $company_id=$arr['company_id'];
        $medical = new $this->model;
        $medical->fill($arr);
        $medical->medical_type_id = $medical_type_id;
        $medical->company_id = $company_id;
        return $medical->save();
    }

    /**
     * @param $id
     * @return bool
     */
    public function deleteById($id): bool
    {

        $medical = $this->model->find($id);

        if ($medical != null) {
            return $this->model->where('id', $id)->delete();
        }
        return false;
    }

    public function getMedicalList()
    {
        $medicalList = $this->model()::select('id', 'display_name')->orderBy('display_name')->get();
        return $medicalList;
    }
}
