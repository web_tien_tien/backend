<?php

namespace App\Http\Requests\Ctv\DiseaseType;

use Illuminate\Foundation\Http\FormRequest;

class StoreDiseaseTypeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:255|unique:sick_type,name',
            'description' => 'max:255'
        ];
    }

    /**
     * assign attributes
     * @return array
     */
    public function attributes()
    {
        return [
            'name' => trans('fields.sick_type_name'),
            'description' => trans('fields.sick_type_description')
        ];
    }
}
