export const generateTableAction = (type, action, colorInput, iconInput, titleInput) => {
    let color = '', icon = '', title = ''

    switch
        (type) {
        case 'edit':
            color = 'primary'
            icon = 'la-edit'
            title = 'Chỉnh sửa'
            break
        case 'delete':
            color = 'danger'
            icon = 'la-trash'
            title = 'Xóa'
            break
        default :
            color = colorInput
            icon= iconInput
            title = titleInput
            break
    }

    return '<a href="javascript:;" data-action="' + action + '" class="m-portlet__nav-link btn m-btn m-btn--hover-' + color + ' m-btn--icon m-btn--icon-only m-btn--pill table-action" title="' + title + '">\n' +
        '   <i class="la ' + icon + '"></i>\n' +
        '</a>'
}

export const htmlEscapeEntities = function (d) {
    return typeof d === 'string' ?
        d.replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;') :
        d
}
