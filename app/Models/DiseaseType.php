<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Model\Sick;

class DiseaseType extends Model
{
    protected $table = 'sick_type';

    protected $fillable = [
        'name', 'description'
    ];

    public function sicks()
    {
        return $this->hasMany(Sick::class, 'sick_type_id', 'id');
    }
}
