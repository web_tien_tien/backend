const Dashboard = () => import('~/pages/Dashboard.vue').then(m => m.default || m)
const NotFound = () => import('~/pages/errors/404.vue').then(m => m.default || m)

const Login = () => import('~/pages/auth/Login.vue').then(m => m.default || m)

/*
 |--------------------------------------------------------------------------
 | Admin
 |--------------------------------------------------------------------------
 */

const Admin = () => import('~/pages/admin/Admin.vue').then(m => m.default || m)
const Ctv = () => import('~/pages/ctv/ctv.vue').then(m => m.default || m)
const General = () => import('~/pages/general/General.vue').then(m => m.default || m)
const User = () => import('~/pages/admin/User/User.vue').then(m => m.default || m)
const Role = () => import('~/pages/admin/Role/Role.vue').then(m => m.default || m)
const Permission = () => import('~/pages/admin/Permission/Permission.vue').then(m => m.default || m)
const ActionsLog = () => import('~/pages/admin/ActionsLog/ActionsLog.vue').then(m => m.default || m)
const MedicalType = () => import('~/pages/ctv/MedicalType/MedicalType.vue').then(m => m.default || m)
const DiseaseType = () => import('~/pages/ctv/DiseaseType/DiseaseType.vue').then(m => m.default || m)
const Company = () => import('~/pages/ctv/Company/Company.vue').then(m => m.default || m)
const Sick = () => import('~/pages/ctv/Sick/Sick.vue').then(m => m.default || m)
const Medical = () => import('~/pages/ctv/Medical/Medical.vue').then(m => m.default || m)
const MyProfile = () => import('~/pages/general/MyProfile/MyProfile.vue').then(m => m.default || m)

import {
    DASHBOARD_ACCESS,
    LOG_ACCESS,
    PERMISSION_MANAGEMENT,
    ROLE_MANAGEMENT,
    USER_MANAGEMENT,
    CAN_ORDER,
    MEDICAL_TYPE_MANAGEMENT,
    MEDICAL_MANAGEMENT,
    SICK_TYPE_MANAGEMENT,
    SICK_MANAGEMENT,
    COMPANY_MANAGEMENT
} from '~/constants/permissions';

export const adminRoutes = {
    index: {path: '/', redirect: {name: 'admin.user'}},
    user: {path: 'user', name: 'admin.user', component: User, meta: {permission: USER_MANAGEMENT}},
    role: {path: 'role', name: 'admin.role', component: Role, meta: {permission: ROLE_MANAGEMENT}},
    permission: {path: 'permission', name: 'admin.permission', component: Permission, meta: {permission: PERMISSION_MANAGEMENT}},
    log: {path: 'log', name: 'admin.log', component: ActionsLog, meta: {permission: LOG_ACCESS}},

}

export const ctvRoutes = {
    medical_type: {path: 'medical_type', name: 'ctv.medical_type', component: MedicalType, meta: {permission: MEDICAL_TYPE_MANAGEMENT}},
    disease_type: {path: 'disease_type', name: 'ctv.disease_type', component: DiseaseType, meta: {permission: SICK_TYPE_MANAGEMENT}},
    company: {path: 'company', name: 'ctv.company', component: Company, meta: {permission: COMPANY_MANAGEMENT}},
    sick: {path: 'sick', name: 'ctv.sick', component: Sick, meta: {permission: SICK_MANAGEMENT}},
    medical: {path: 'medical', name: 'ctv.medical', component: Medical, meta: {permission: MEDICAL_MANAGEMENT}},
}

export const generalRoutes = {
    my_profile: {path: 'my_profile', name: 'general.my_profile', component: MyProfile, meta: {}},
}

export default {
    dashboard: {path: '/', name: 'dashboard', component: Dashboard, meta: {}},

    login: {path: '/login', name: 'login', component: Login},

    admin: {
        path: '/admin',
        component: Admin,
        children: Object.values(adminRoutes)
    },

    ctv: {
        path: '/ctv',
        component: Ctv,
        children: Object.values(ctvRoutes)
    },

    general: {
        path: '/general',
        component: General,
        children: Object.values(generalRoutes)
    },

    not_found: {path: '*', name: 'not_found', component: NotFound}
}

// permission: 'user.management'