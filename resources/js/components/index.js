import Vue from 'vue'
import Child from './Child'
import { HasError, AlertError, AlertSuccess } from 'vform'

/*
 |--------------------------------------------------------------------------
 | Common
 |--------------------------------------------------------------------------
 */
import Dropdown from './common/Dropdown'
import DynamicTag from './common/DynamicTag'
import VButton from './common/VButton'
import Alert from './common/Alert'
import Scrollbar from './common/Scrollbar'
import Portlet from './common/Portlet'
import DataTable from './common/DataTable'
import Modal from './common/Modal'
import FormControl from './common/FormControl'
import Select2 from './common/Select2'
import ClipLoader from 'vue-spinner/src/ClipLoader'

/*
 |--------------------------------------------------------------------------
 | Elements
 |--------------------------------------------------------------------------
 */
import Notification from './elements/Notification'
import LanguageChosen from './elements/LanguageChosen'
import ProfileActions from './elements/ProfileActions'

// Components that are registered global.
[
    Child,
    HasError,
    AlertError,
    AlertSuccess,
    Dropdown,
    DynamicTag,
    VButton,
    Alert,
    Scrollbar,
    Portlet,
    DataTable,
    Modal,
    FormControl,
    Select2,
    ClipLoader,

    Notification,
    LanguageChosen,
    ProfileActions,
].forEach(Component => {
    Vue.component(Component.name, Component)
})
